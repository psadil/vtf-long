
Root on laptop D:\git\fMRI\VTF-long
Root at scanner C:\Users\CMAP\Documents\My Experiments\psadil\VTF-high

# VTF-long/high

stimulus protocol for longer VTF stimulation, 2018-2019

# At Scanner

The 3 sessions per participant are largely similar

## Session 1

### Intake Room

Bring participant into intake room. There, have them sign (blank copies of 
these forms live in Patrick's desk in LSL office, bottom drawer)
1) Consent Form
2) NDA GUID Form
3) Screening Form 

Tell participant to demetal (leaving stuff in lockers), and use the restroom.
While that's happening, bring the screening + consent forms to MR operator to look over

Still in the intake room, overview both tasks for the participant, e.g.,

main('subject', sub, 'run', 0, 'exp', 'VTF', 'using_tracker', false, 'gamma', false, 'skipsynctests',2)
main('subject', sub, 'run', 0, 'exp', 'multibar', 'using_tracker', false, 'gamma', false, 'skipsynctests',2) 

(experiments can be triggered with the space bar once the fixation dot is
visible. end an experiment with crtl+c)

use magnetic wand on participant to check for metal (rivets in pants okay)

### Scanner

Participant will put on earplugs and hairnet. help Elena put them in the 
scanner

Check that input is being received correctly with the button box (NAR_HID)

Remind participant that now is the time to get comfortable, prior to the first
eyetracking session.

Remind that there's an initial phase of not doing much (bring up youtube
for them to watch something)

prior to the first task, go over instructions again. Remind about closing
eyes during the blank periods (TR is 1 second, and the shortest gap between
trials is ~8, so they can definitely close their eyes for 3-4 TRs)

Typical scanning session written out in `example_starts.m` script. The idea
is to, for each scanning session, set the variable `sub` at the start of the
script to the appropriate number, and resave with a BIDS compatable file 
name structure. This will serve as another record of exactly which scans
were run in which order. Each line runs a different scan in an order that is
matched to the base stimulus protocol at the hMRC scanner. At the end of a 
session, save the exact script that was run along with the events/* files 
that were generated during scanning, modified with any comments about 
excluded or additional scans that were run. This is the record in a 'lab
notebook,' hopefully making it even easier to reconstruct down the line 
exactly what happened for each participant. Though, of course the better 
scenario would be if exactly the same thing happened for each paticipant.

Note that it's a good idea to check in with the participant between runs;
keeping in contact with them can help them feel at ease

Always export the imaging data twice, once that's not anonymized and once 
that's anonymized. For the anonymized data, click the 'anonymously box'
and use a participant name like the following

YYYYMMDD_VTF_sub##_ses#

So, yearmonthday), VTF, the subject's number, and the session number.
For example, the last session of sub-02 was 

190506_VTF_sub02_ses3


### Summary

1) Greet participant. Have them fill out forms
2) Have them go through partial run of each task (run 0). Includes
demographics questionnare
3) De-metal, placing stuff in locker
4) Set them up in the scanner.
5) Set up computer, checking that Button Response Box is set to HID NAR 12345
6) Run initial eyetracker calibration (want to confirm that eyetracker can 
calibrate with their position)
7) Initial anatomical + fieldmapping scans
7.5) Align image matrix with calcarine sulcus
8) Run step by step through scans on example_starts.m
9) Copy generated behavioral data + images back to SSD. 
10) Profit! (and pay participant) money is in top drawer in lsl office

## Session 2-3

Almost identical to session 1, but no need to run through practice.

# after scanner

Plug laptop back in to power, larger external, and smaller SSD external.
(no need to copy files off of SSD, so long as they're stored in SSD and the
SSD is plugged in to the laptop)
