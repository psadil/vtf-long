
diary sub-01_ses-2_diary.txt
sub = 1;

main('subject', sub, 'run', 3, 'exp', 'calibrate') 

% starts with 
% 1) Fieldmapping scan (required to begin for aligning of scan slices)
% 2) anatomical scan

main('subject', sub, 'run', 4, 'exp', 'calibrate') 

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 2, 'exp', 'multibar') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 7, 'exp', 'VTF', 'using_tracker', false)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 8, 'exp', 'VTF')

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 9, 'exp', 'VTF')

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 2, 'exp', 'wedgering') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 10, 'exp', 'VTF') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 11, 'exp', 'VTF') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 12, 'exp', 'VTF') 

diary off
