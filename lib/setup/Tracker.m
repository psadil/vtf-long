classdef Tracker < handle
    
    properties
        using_tracker logical = false
        
        filename char = ''
        background_img_filename = fullfile('stimuli','tracker_background.bmp')
        el
    end
    
    methods
        function obj = Tracker(using_tracker, filename, window)            
            obj.using_tracker = using_tracker;
            obj.filename = filename;
            calibrate(obj, window);
        end
        
        function calibrate(obj, window)
            if obj.using_tracker
                % Provide Eyelink with details about the graphics environment
                % and perform some initializations. The information is returned
                % in a structure that also contains useful defaults
                % and control codes (e.g. tracker state bit and Eyelink key values).
                obj.el = EyelinkInitDefaults(window.pointer);
                
                if ~EyelinkInit(0, 1)
                    error('\n Eyelink Init aborted \n');
                end
                
                %Reduce FOV for calibration and validation. Helpful with
                %the narrow FOV at the scanner
                Eyelink('Command','calibration_area_proportion = 0.5 0.5');
                Eyelink('Command','validation_area_proportion = 0.5 0.5');
                
                % open file to record data to
                i = Eyelink('Openfile', obj.filename);
                if i ~= 0
                    error('\n Eyelink Init aborted \n');
                end
                
                % Setting the proper recording resolution, proper calibration type,
                % as well as the data file content;
                Eyelink('Command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, window.winRect(3)-1, window.winRect(4)-1);
                Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, window.winRect(3)-1, window.winRect(4)-1);
                % set calibration type.
                Eyelink('Command', 'calibration_type = HV5');
                
                % set EDF file contents using the file_sample_data and
                % file-event_filter commands
                % set link data thtough link_sample_data and link_event_filter
                Eyelink('Command', 'file_event_filter = RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
                Eyelink('Command', 'link_event_filter = RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT');
                
                % check the software version
                % add "HTARGET" to record possible target data for EyeLink Remote
                Eyelink('command', 'file_sample_data  = RIGHT,GAZE,HREF,GAZERES,AREA,HTARGET,STATUS,INPUT');
                Eyelink('command', 'link_sample_data  = RIGHT,GAZE,HREF,GAZERES,AREA,HTARGET,STATUS,INPUT');
                
                % make sure we're still connected.
                if Eyelink('IsConnected')~=1
                    error('\n Eyelink Init aborted \n');
                end
                
                % set sample rate in camera setup screen
                Eyelink('Command', 'sample_rate = %d', 1000);
                
                % opens up main calibration scheme
                EyelinkDoTrackerSetup(obj.el);
            end
        end
        
        function status = eyelink(obj, varargin)
            % sneaky funciton that calls main Eyelink routines only when
            % this tracker object is 'on'. As a test, see
            % lib/scrap/expanding.m function.
            status = [];
            if obj.using_tracker
                if nargin==2
                    if strcmp(varargin{1}, 'StopRecording') || ...
                            strcmp(varargin{1}, 'Shutdown') ||...
                            strcmp(varargin{1}, 'SetOfflineMode')
                        Eyelink(varargin{:});
                    else
                        status = Eyelink(varargin{:});
                    end
                else
                    status = Eyelink(varargin{:});
                end
            end
        end
        
        function startup(obj)
            
            % Must be offline to draw to EyeLink screen
            obj.eyelink('SetOfflineMode');
            
            % clear tracker display and draw background img to host pc
            obj.eyelink('Command', 'clear_screen 0');
            
            % image file should be 24bit or 32bit bitmap
            % parameters of ImageTransfer:
            % imagePath, xPosition, yPosition, width, height, trackerXPosition, trackerYPosition, xferoptions
            % VERY SLOW. Should only be done when not recording
            %             obj.eyelink('ImageTransfer', obj.background_img_filename);
            
            %            obj.eyelink('command', 'draw_cross %d %d', 0, 0);
            obj.eyelink('command', 'draw_cross %d %d', 1920/2, 1080/2);
            
            % give image transfer time to finish
            WaitSecs(0.1);
        end
        
        
        function delete(obj)
            
            % End of Experiment; close the file first
            % close graphics window, close data file and shut down tracker
            obj.eyelink('StopRecording');
            WaitSecs(0.1); % Slack to let stop definitely happen
            obj.eyelink('SetOfflineMode'); % apparently auto waits for mode change
            
            obj.eyelink('CloseFile');
            WaitSecs(0.1);
            
            obj.eyelink('ReceiveFile', obj.filename, fullfile(pwd,'events'), 1);
            WaitSecs(0.2);
            obj.eyelink('Shutdown');
        end
        
    end
end

