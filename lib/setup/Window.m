classdef Window < handle
    % Window handles opening and closing of screen
    
    properties (Constant)
        screenNumber = 0;
        
        black = BlackIndex(Window.screenNumber)
        white = WhiteIndex(Window.screenNumber)
        gray = GrayIndex(Window.screenNumber)
        background = Window.gray
        TextFont = 'Courier New'
        TextSize = 24
        TextStyle = 1
        TextColor = Window.white
        
        % how many frames of slack when scheduling flips (between 0-1)
        slack = 0.5;
        
    end
    
    properties
        view_distance_cm
        oldRes
        screen_w_cm
        screen_h_cm
        pointer
        winRect
        xCenter
        yCenter
        width
        height
        
        framerate
        ifi
        
        loadgamma = false
        gamma
        
    end
    
    methods
        function obj = Window(fmri, varargin)
            [screen_w_mm, screen_h_mm] = Screen('DisplaySize', 0);
            obj.screen_w_cm = screen_w_mm / 10;
            obj.screen_h_cm = screen_h_mm / 10;
            obj.view_distance_cm = 12 * 2.54;
            
            if (nargin > 0)
                if fmri
                    % https://www.crsltd.com/tools-for-functional-imaging/mr-safe-displays/boldscreen-32-lcd-for-fmri/nest/boldscreen-32-technical-specification#npm
                    % https://www.umass.edu/ials/sites/default/files/hmrc_tn_bold_screen_view_angle.pdf
                    obj.screen_w_cm = 69.84;
                    obj.screen_h_cm = 39.29;
                    obj.view_distance_cm = 137;
                end
                if (nargin > 1)
                   obj.loadgamma = varargin{1};
                end
            end
        end
        
        function open(obj, skipsynctests, debuglevel)
            
            %             Screen('Preference', 'ConserveVRAM', 4096);
            PsychImaging('PrepareConfiguration');
            PsychImaging('AddTask', 'General', 'FloatingPoint16Bit');
            PsychImaging('AddTask', 'General', 'UseFastOffscreenWindows')
            
            Screen('Preference', 'SkipSyncTests', skipsynctests);
            switch debuglevel
                case 1
                    [obj.pointer, obj.winRect] = ...
                        PsychImaging('OpenWindow', obj.screenNumber, obj.background);
                case 10
                    PsychDebugWindowConfiguration(0, .5)
                    [obj.pointer, obj.winRect] = ...
                        PsychImaging('OpenWindow', obj.screenNumber,obj.background);
                case 0
                    ListenChar(-1);
                    HideCursor;
                    [obj.pointer, obj.winRect] = ...
                        PsychImaging('OpenWindow', obj.screenNumber, obj.background);
            end
            
            % need GLSL for procedural definition of gratings
            AssertGLSL;
            
            % Turn on blendfunction for antialiasing of drawing dots
            Screen('BlendFunction', obj.pointer, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
            
            topPriorityLevel = MaxPriority(obj.pointer);
            Priority(topPriorityLevel);
            
            % define some landmark locations to be used throughout
            [obj.xCenter, obj.yCenter] = RectCenter(obj.winRect);
            obj.width = RectWidth(obj.winRect);
            obj.height = RectHeight(obj.winRect);
            
            % Get some info about our window timing
            obj.ifi = Screen('GetFlipInterval', obj.pointer);
            obj.framerate = FrameRate(obj.pointer);
            
            % Font Configuration
            Screen('TextFont', obj.pointer, obj.TextFont);
            Screen('TextSize', obj.pointer, obj.TextSize);
            Screen('TextStyle', obj.pointer, obj.TextStyle);
            Screen('TextColor', obj.pointer, obj.TextColor);
            
            if obj.loadgamma
                BackupCluts();
                % gamma tables ought to be unset when window object is
                % deleted due to call of sca in delete method
                obj.gamma = 'stimuli/gammaTable-hmrc-factory-rgb.mat';
                gammatable = importdata(obj.gamma, 'gammaTable');
                Screen('LoadNormalizedGammaTable', obj.pointer, gammatable);
            else
                obj.gamma = 'unset';
            end
            
        end
        
        function delete(obj) %#ok<INUSD>
            ListenChar(0);
            Priority(0);
            sca;
        end
        
        function pixels = deg2pix(obj, desired_degrees )
            % converts visual angle into pixels
            
            % visual angle of the whole screen
            max_degrees = rad2deg(2*atan2(obj.screen_w_cm/2, obj.view_distance_cm));
            pixels_per_degree = obj.winRect(3) / max_degrees;
            pixels = pixels_per_degree * desired_degrees;
        end
    end
    
end

