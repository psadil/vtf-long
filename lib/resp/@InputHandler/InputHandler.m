classdef InputHandler < handle
    %InputHandler receive and parse inputs of various kinds from different
    %sources
    
    properties (Constant)
        device = [];
    end
    
    properties
        keys_pressed = []
        rt = []
        trial_ender = false
        responder = 'user'
        keys = Keys()
    end
    
    properties (SetObservable, AbortSet)
        % allow for listening of press_times so that experiment object
        % can update response + rt (press_times is set after
        % keys_pressed in function check_keys)
        press_times
    end
    
    methods
        function obj = InputHandler(responder)
            if nargin > 0
                obj.responder = responder;
            end            
        end
        
        prepare(obj, codes)
        shutdown(obj)
        when = wait_for_start(obj)
        check_keys(obj);
    end
    
    
end

