function response = process_single_key(obj)
obj = check_keys(obj);
response = KbName(obj.keys_pressed);
end
