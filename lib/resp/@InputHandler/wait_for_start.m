function when = wait_for_start(obj)
obj.prepare(obj.keys.start)
while 1
    [ pressed, timespressed] = KbQueueCheck(obj.device);
    if pressed
        % find the keycode for the keys pressed since last check
        whichpressed = find(timespressed);
        % sort the recorded press time to find their linear position
        [~, ind] = sort(timespressed(timespressed~=0));
        % Arrange the keycodes according to the order they were pressed
        keys_pressed = whichpressed(ind);
        press_times = timespressed;
        when = press_times(keys_pressed(end));
        break
    end
end
obj.shutdown();
end
