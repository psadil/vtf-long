T = table();

T.contrast = Shuffle(repelem([0.2, 0.8], 7))';
T.orientation = Shuffle(repelem(linspace(0,360-(360/7),7), 2))';
T.run = zeros(size(T,1),1);
T.subject = ones(size(T,1),1)*99;

% determines length of stimulus presentation
T.n_stim_flip = ones(size(T,1),1)*40;
T.n_isi_flip = ones(size(T,1),1)*10;

% on which flips will the event occur?
T.freq_change_flip = randsample(10:30, size(T,1))';

% filled in during stimulus presentation
T.trial_start = NaN(size(T,1),1);
T.trial_end = NaN(size(T,1),1);

% filled in as participant responds (response is cell, which needs special
% handling during reading and so is created when preparing datatable)
T.rt = NaN(size(T,1),1);

% these are calculated from trial_*
T.onset = NaN(size(T,1),1);
T.duration = NaN(size(T,1),1);


writetable(T,'task-vtf_blocking-demo.csv')

