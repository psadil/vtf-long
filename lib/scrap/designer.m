
tmp = repelem(0:18, 6);
tmp = [tmp,tmp,tmp];

deconvM = DconvMTX(tmp, numScan, Inp.nSTYPE, Inp.durHRF, Inp.ISI, Inp.TR, Inp.dT);
EAmp = AmpEfficiency(deconvM, Inp.basisHRF, Inp.nSTYPE, whiteM, Inp.CZ, Inp.Opt, Inp.Nonlinear);
EAmp

tmp = repelem(0:18, 10);
tmp = [tmp,tmp,tmp];

deconvM = DconvMTX(tmp, numScan, Inp.nSTYPE, Inp.durHRF, Inp.ISI, Inp.TR, Inp.dT);
EAmp = AmpEfficiency(deconvM, Inp.basisHRF, Inp.nSTYPE, whiteM, Inp.CZ, Inp.Opt, Inp.Nonlinear);
EAmp

n_null = 10;
tmp = repmat(1:14, [5, 1]);
tmp = [zeros(n_null/2, 14); tmp; zeros(n_null/2, 14)];
tmp = [tmp(:); tmp(:)];
length(tmp)

deconvM = DconvMTX(tmp, numScan, Inp.nSTYPE, Inp.durHRF, Inp.ISI, Inp.TR, Inp.dT);
EAmp = AmpEfficiency(deconvM, Inp.basisHRF, Inp.nSTYPE, whiteM, Inp.CZ, Inp.Opt, Inp.Nonlinear);
EAmp



