
PsychDefaultSetup(2);

% run without gamma correction

demo = Demo();
demo.grating.contrast = .8;
demo.grating.orientation = 90;
img = run_and_get_stimulus(demo);
sca;
delete(demo);

demo = Demo();
demo.grating.contrast = .8;
img = run_and_get_stimulus(demo);
sca;
delete(demo);


% checks on images
imshow(img);
max(img(:))
min(img(:))

demo = Demo(true);
demo.grating.contrast = .8;
img2 = run_and_get_stimulus(demo);
sca;
delete(demo);

imshow(img2);
max(img2(:))
min(img2(:))


% PsychDefaultSetup(2);
% addpath(genpath('lib'));
% demo = Demo();
% 
% rotate(demo);
% 
% delete(demo);
% rmpath(genpath('lib'));



