% inherits from prepareimages_categoryC.m and prepareimages_wn.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXECUTIVE SUMMARY

% RETINOTOPY:
% 768 px, 15�, 51.2 px/�.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SETUP

%%%%%%%% FOR REGULAR

res = 768;
totalfov = 15;                 % total number of degrees for FOV
rfov = totalfov/2;             % radius of FOV in degrees

  % SPECIAL VERSION (workspace_retinotopyCaltsmash1080px.mat)
  res = 1080;
  totalfov = 15 * res/768;                 % total number of degrees for FOV
  rfov = totalfov/2;             % radius of FOV in degrees

% make basic circle
fig = figure;
drawsector(0,0,2*pi,0,1,[1 1 1],[0 0 0]);
basiccircle = renderfigure(res,2);
close(fig);

%%%%%%%% FOR Caltsmash1080px

nikoims = permute(reshape(loadmulti('~/ext/stimuli/kriegeskorte/colorimages.mat','images'),[92 175 175 3]),[2 3 4 1]);
nikomasks = reshape(loadmulti('~/ext/stimuli/kriegeskorte/masks.mat','masks')',[175 175 92]);
images = {};
images{1} = uint8(254*drawmultiscale(res,100, 2.^(1.5:-.5:-2), 2*2,0.1,nikoims,nikomasks));  % NOTICE THESE CHANGES
load('~/ext/stimulusfiles/workspace_retinotopy.mat','eccs');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCT maskimages [VERSION FOR C]

% initialize
maskimages = {};

%%%%%%%%%%% 90� wedge CCW, 32 s, 15-Hz steps [1] [to achieve CW, handle at presentation stage]

% constants
numsteps = 32*15;
wedgesize = pi/2;

% calc
angs = linspacecircular(0,2*pi,numsteps);

% make apertures
apertures = zeros(res,res,numsteps,'single');
for pp=1:length(angs)
  fig = figure;
  drawsector(0,angs(pp),angs(pp)+wedgesize,0,1,[1 1 1],[0 0 0]);
  apertures(:,:,pp) = renderfigure(res,2);
  close(fig);
end

% record
maskimages{end+1} = apertures;

%%%%%%%%%%% 1/4 ring expand, 28 s, 15-Hz steps [2] [to achieve contract, handle at presentation stage]

% some notes:
% >> 12,13,14,  25, 36, ... 10,13  11,13,  12,13
% >> 1 whole to get up to 12.
% >> so need 15 total to traverse
% however, we want to start at the first step (too small otherwise).  so still 14.

% NOTE: inherit eccs from above!

% C:
framesperstep = 28*15/14;  % 28 s, 15 frames per sec.  in that time, we are trying to move through 14 steps.
newix = linspacefixeddiff(1,1/30,30+28*15);  % if each frame moves 1/30 step, then we need this many,
                                             % pertaining to the leading edge.  note that this will move
                                             % us outside of the original 1 to 13 range, but that's ok (see below).
                                             % note that the first 30 are bogus (freebies).
eccsuse = interp1(1:length(eccs),eccs,newix,'cubic');
figure;plot(newix,eccsuse,'ro-');
numsteps = 28*15;

% make apertures
apertures = zeros(res,res,numsteps,'single');
for pp=1:numsteps+30
  if pp<=30
    continue;
  end
  beginecc = max(1,pp - 3*framesperstep);  % the ring is 3 steps wide
  endecc = min(length(eccsuse),pp);
  fprintf('[%d %d]\n',beginecc,endecc);
  fig = figure;
  drawsector(0,0,2*pi,eccsuse(beginecc),min(eccsuse(endecc),1),[1 1 1],[0 0 0]);  % NOTICE the min 1 here!
  apertures(:,:,pp - 30) = renderfigure(res,2);
  close(fig);
end

% record
maskimages{end+1} = apertures;

%%%%%%%%%%% 1/4-radius bars sweeping four directions, 28 s steps [3,4,5,6]

% left to right, DL to UR, D to U, DR to UL
barsweepdirs = [0 pi/4 pi/20 3*pi/4];
for qqq=1:length(barsweepdirs)

  % constants
  numsteps = 28*15;
  barwidth = 1/8;  % total is 1, so we need to divide by 8

  % calc
  xposes = linspace(-cos(barsweepdirs(qqq))*(.5 + barwidth/2), ...
                     cos(barsweepdirs(qqq))*(.5 + barwidth/2),28*15+1);  % notice +1 (begin and end are pure blanks)
  yposes = linspace(-sin(barsweepdirs(qqq))*(.5 + barwidth/2), ...
                     sin(barsweepdirs(qqq))*(.5 + barwidth/2),28*15+1);

  % make apertures
  apertures = zeros(res,res,numsteps,'single');
  for pp=1:numsteps
    fig = figure;
    drawbar(0,xposes(pp),yposes(pp),barsweepdirs(qqq)+pi/2,barwidth,10,[1 1 1],[0 0 0]);
    temp = renderfigure(res,2);
%     if ~allzero(max(temp(:)))
%       temp = normalizemax(temp,0);
%       fprintf('%d, %d, weird: %.10f\n',qqq,pp,max(temp(:)));
%     end
    apertures(:,:,pp) = temp .* basiccircle;
    mnval = min(flatten(apertures(:,:,pp)));
    mxval = max(flatten(apertures(:,:,pp)));
    if mnval < 0 || ...
       mnval > .01 || ...
       mxval < .99 || ...
       mxval > 1.01
      fprintf('%d, %d\n',qqq,pp);
      if pp~=1
        apertures(:,:,pp) = normalizemax(apertures(:,:,pp),0);  % THIS IS REALLY WEIRD. happens only for qqq=3.
      end
    end
    close(fig);
  end

  % record
  maskimages{end+1} = apertures;

end

%%%%%%%%%%%%%%%%% SAVE AND INSPECT

maskimages = cellfun(@single,maskimages,'UniformOutput',0);

% save disk space!
clear apertures nikoims nikomasks pattern temp wtf maskimages0;  % save disk space!

% starting with C did this:
for p=1:length(maskimages)
  maskimages{p} = uint8(maskimages{p}*255);
end

% and for C (and workspace_retinotopyCaltsmash1080px.mat) did this:  [CAREFUL, THIS IS DESTRUCTIVE!]
maskimages = cat(3,maskimages{:});
save('~/ext/stimulusfiles/workspace_retinotopyCaltsmash1080px.mat','-v7.3');  % this was a one-off, need to check for sanity.
