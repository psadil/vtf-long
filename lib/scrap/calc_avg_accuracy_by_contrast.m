function avg = calc_avg_accuracy_by_contrast(subject, run)

T = readtable(fullfile('events',...
    sprintf('sub-%02d_task-VTF_run-%02d_data.csv', subject,run)));

correct = (contains(T.response, '1') & T.freq_change_feature==2) | ...
    (contains(T.response, '2') & T.freq_change_feature==1);
low = T.contrast == min(T.contrast);
high = T.contrast == max(T.contrast);

avglow = mean(correct(low));
avghigh = mean(correct(high));

avg = [avglow, avghigh];
end

