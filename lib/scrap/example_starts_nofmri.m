% script used for debugging purposes only (e.g., running on laptop outside
% of scanner)

sub = 99999;

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 1, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2) 

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 2, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2) 

% retinotopy tasks require 5 minutes
% run 1, 93=multibar
runretinotopy_vtf('subnum',sub,'runnum',1,'expnum',93, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2)

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 3, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2) 

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 4, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2) 

% run 2, 94=wedgeringmash, 5 minutes
runretinotopy_vtf('subnum', sub, 'runnum', 1, 'expnum',94, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2)

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 5, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2) 

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 6, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2) 

