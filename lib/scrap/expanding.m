function  expanding(varargin)
% example run
% t = {[1],[2]};
% expanding(t, 'UniformOutput', false)
% expanding(t, 'UniformOutput', true)
% notice that results are different!

cellfun(@(x) x==true, varargin{:});

end

