classdef DemoWedgeRing < handle
    % DEMO
    % the difference between a demo and an experiment is that demos will
    % not generally have a data object associated with them.
    
    properties(Constant)
        skipsynctests = 2
        debuglevel = 1
        first_flip_showing = 1
    end
    
    properties
        n_flips = 500
        window
        stimulus
    end
    
    methods
        function obj = DemoWedgeRing()
            obj.window = Window(true);
            obj.stimulus = WedgeRing();
        end
        
        function run(obj)            
            open(obj.window, obj.skipsynctests, obj.debuglevel);
            prepare(obj.stimulus, obj.window);
            run(obj.stimulus, obj.window, obj.n_flips,...
                'first_flip_showing',obj.first_flip_showing);   
        end
    end
end

