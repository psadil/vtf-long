classdef Demo < handle
    % DEMO
    % the difference between a demo and an experiment is that demos will
    % not generally have a data object associated with them. 
    
    properties(Constant)
        skipsynctests = 2
        debuglevel = 0
    end
    
    properties
        n_flips = 10
        window
        grating
        loadgamma = false
    end
    
    methods
        function obj = Demo(varargin)            
            if (nargin > 0)
               obj.loadgamma = varargin{1}; 
            end
            obj.window = Window(true, obj.loadgamma);
            obj.grating = Grating();
        end
        
        function run(obj)            
            open(obj.window, obj.skipsynctests, obj.debuglevel);
            vbl = prepare(obj.grating, obj.window);
            run(obj.grating, obj.window, obj.n_flips, 'vbl', vbl);
        end
        
        function img = run_and_get_stimulus(obj)
            open(obj.window, obj.skipsynctests, obj.debuglevel);
            vbl = prepare(obj.grating, obj.window);
            obj.grating.freq_cpp = obj.grating.freq_cpp_deviant(1);
            run(obj.grating, obj.window, obj.n_flips, 'vbl', vbl);
            img = Screen('GetImage', obj.window.pointer,[],'backBuffer',1);
%             imwrite(img, 'stimuli/tracker_background.bmp');            
        end
        
        % function to confirm how the orientations will translate into
        % different gratings. 0 and 180 both mean vertical. 90 means
        % horizontal
        function rotate(obj)
            orientations = 0:30:180;
            open(obj.window, obj.skipsynctests, obj.debuglevel);
            vbl = prepare(obj.grating, obj.window);
            for o = 1:length(orientations)
                obj.grating.orientation = orientations(o);
                vbl = run(obj.grating, obj.window, obj.n_flips, 'vbl', vbl);
            end
            ListenChar(0);
            Priority(0);
            ShowCursor();
            Screen('CloseAll');
        end        
    end
end

