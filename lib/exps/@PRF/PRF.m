classdef PRF < Experiment
    
    properties
        exp
        filenameprefix
        data
        n_trial
        
        stimulus
        
        total_sec = 300
        warmup_sec
        
        fixation_counter = 1
        fixations_which
        fixations_when
        response_counter = 1
        responses_when
    end
    
    methods
        function obj = PRF(input)
            obj@Experiment(input);
            
            obj.exp = input.exp;
            
            obj.filenamechecker();
            
            switch obj.exp
                case 'multibar'
                    obj.stimulus = MultiBar();
                    obj.warmup_sec = 16;
                case 'wedgering'
                    obj.stimulus = WedgeRing();
                    obj.warmup_sec = 22;
            end
            
            obj.data = prepare_data(obj);
            obj.n_trial = size(obj.data,1);
            
            % listen for these changes to update relevant columns in data
            addlistener(obj.stimulus, 'first_stim_on_vbl', 'PostSet',...
                @(src,event)obj.log_stim_on(src,event) );
            addlistener(obj.stimulus, 'stim_off_vbl', 'PostSet',...
                @(src,event)obj.log_stim_off(src,event) );
            
            addlistener(obj.stimulus, 'fixation_change_when', 'PostSet',...
                @(src,event)obj.log_fix_change(src,event) );
            
            % listen for responses provided, which is used to update data
            addlistener(obj.input_handler, 'press_times', 'PostSet',...
                @(src,evnt)obj.record_response(src,evnt) );            
        end
        
        function delete(obj)
            obj.stimulus.circ_tex = [];
            obj.stimulus.stimulus_tex = [];
            obj.stimulus.xy = [];
            if ~isempty(obj.filenameprefix)
                % save these values as .mat since responses can strictly
                % occur throughout entire experiment (not limited to once
                % per fixation change)
                fixations = struct();
                fixations.which = obj.fixations_which(~isnan(obj.fixations_which));
                fixations.when = obj.fixations_when(~isnan(obj.fixations_which)) - obj.exp_start;
                fixations.responses = obj.responses_when(~isnan(obj.responses_when)) - obj.exp_start;
                save([obj.filenameprefix,'_fixations.mat'], 'fixations');
            end
        end
                
        % methods defined in external files
        record_response(obj, ~, ~)
        log_fix_change(obj,~,~)
        update_stimulus(obj, ~, ~)
        data = prepare_data(obj)
        vbl = run_warmup(obj, vbl)
    end
end

