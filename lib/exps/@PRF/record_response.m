function record_response(obj, ~, ~)

obj.responses_when(obj.response_counter) = max(obj.input_handler.press_times);
obj.response_counter = obj.response_counter + 1;

end
