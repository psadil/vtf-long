function data = prepare_data(obj)

data = readtable(fullfile(obj.input.root, 'task-prf_blocking.csv'));

% grab only either 'multibar' or 'wedgering' runs
data = data(strcmp(data.run_type, obj.exp), :);

switch obj.exp
    case 'multibar'
        data.direction = str2double(data.direction);
end

% could have up to this many fixation changes (once per flip), but most of
% these will be filtered out during saving
n_flips_total = obj.total_sec*obj.stimulus.hz;
obj.fixations_which = NaN(n_flips_total,1);
obj.fixations_when = NaN(n_flips_total,1);
obj.responses_when = NaN(n_flips_total,1);

end

