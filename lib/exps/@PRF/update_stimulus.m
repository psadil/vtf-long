function update_stimulus(obj, ~, ~)

obj.tracker.eyelink('Message', 'TRIALID %d', obj.trial);
obj.tracker.eyelink('Command', 'record_status_message "TRIAL %d"', obj.trial);

% note that we should not need to wait to start recording,
% given that the stimulus will always be drawn a bit later
% (determined by how often phase changes occur)
obj.tracker.eyelink('StartRecording');


obj.stimulus.wedgeorring = obj.data.wedgeorring{obj.trial};

% difference in picking direction is whether we're indexing into a cell or
% a matrix for the direction column.
switch obj.exp
    case 'multibar'
        obj.stimulus.direction = obj.data.direction(obj.trial);
    case 'wedgering'
        obj.stimulus.direction = obj.data.direction{obj.trial};
        obj.stimulus.wedge_transition_ang = 360 / (obj.data.n_stim_flip(obj.trial));
end

end
