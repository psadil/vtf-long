function vbl = run_warmup(obj, vbl)

obj.stimulus.fixation_change_when = vbl;

warmup_end_after = (obj.warmup_sec - (obj.stim_start - obj.trigger_sent)) - 1;
n_warmup_flips = floor(warmup_end_after*obj.stimulus.hz);

obj.input_handler.prepare(obj.input_handler.keys.task);
% note - 1 flip. The blank screen isn't cleared until the first flip after
% this one
vbl = isi(obj.stimulus, obj.window, n_warmup_flips, ...
    'vbl', vbl, 'stim_off_vbl', false, 'input_handler', @()obj.input_handler.check_keys());
obj.input_handler.shutdown();

% final correction before trials actually start
% take the current time, and add the amount of time left over in the warmup
% period, minus flip_after_sec to account for the delay that will happen on
% the next flip
draw_fixation(obj.stimulus, obj.window);
vbl = Screen('flip', obj.window.pointer, vbl + (obj.warmup_sec - (vbl - obj.trigger_sent) - 1/obj.stimulus.hz - 0.5*obj.window.ifi));

end

