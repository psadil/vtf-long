function log_fix_change(obj,~,~)
obj.tracker.eyelink('Message', 'fixation color changed');

obj.fixations_which(obj.fixation_counter) = obj.stimulus.which_fix;
obj.fixations_when(obj.fixation_counter) = obj.stimulus.fixation_change_when;
obj.fixation_counter = obj.fixation_counter + 1;
end