classdef Calibrate < handle
    
    properties
        input
        savedir
        run_id
        
        tracker
        window
        
    end
    
    methods
        function obj = Calibrate(input)            
            
            obj.input = input;
            obj.run_id = input.run_id;
            
            % setup save filepath info
            obj.savedir = fullfile(obj.input.root, 'events');
            if ~exist(obj.savedir, 'dir')
                mkdir(obj.savedir);
            end
            
            % create and open window
            obj.window = Window(input.fMRI, input.gamma);
            open(obj.window, input.skipsynctests, input.debuglevel);
            
            obj.tracker = Tracker(input.using_tracker, ...
                sprintf('%02d%02dCAL.edf', input.subject, obj.run_id), obj.window);
            
        end
        
        function run(obj)
            % do nothing
        end
                
    end
end

