classdef Experiment < handle
    %Experiment Abstract collection of info about general experiment
    
    properties
        savedir
        subject
        run_id
        
        seed
        input
        % moment when trigger signal received from scanner or keyboard
        trigger_sent = NaN
        % moment when Experiment object is being deleted
        exp_end = NaN
        % moment when Experiment object is being created
        exp_start = NaN
        % moment of last flip relating to stimulus presentation (blank)
        stim_end = NaN
        % moment of first flip after trigger being received
        stim_start = NaN
        datetaken
        version
        
        window
        tracker
        trackerflag
        input_handler
        
    end
    
    properties (Abstract)
        data
        exp
        filenameprefix
        n_trial
        warmup_sec
        total_sec
    end
    
    properties (SetObservable, AbortSet)
        trial = 0
    end
    
    methods
        function obj = Experiment(input)
            
            % saving involves converting to a struct, so we turn this
            % warning off for duration of experiment. delete method will
            % turn the warning back on
            warning('off', 'MATLAB:structOnObject');
            
            if nargin > 0
                rng('shuffle');
                scurr = rng; % set up and seed the randon number generator
                obj.seed = scurr.Seed;
                
                % initial call to GetSecs to avoid delay of loading mex
                % file
                GetSecs;
                obj.exp_start = GetSecs; % record the time the experiment began
                obj.datetaken = date();
                obj.version = Experiment.get_version_string('version.txt');
                
                obj.input = input;
                obj.run_id = input.run_id;
                obj.subject = input.subject;
                
                % setup save filepath info
                obj.savedir = fullfile(obj.input.root, 'events');
                if ~exist(obj.savedir, 'dir')
                    mkdir(obj.savedir);
                end
                
                % create and open window
                obj.window = Window(input.fMRI, input.gamma);
                open(obj.window, input.skipsynctests, input.debuglevel);
                
                switch input.exp
                    case 'multibar'
                        obj.trackerflag = 'PMB';
                    case 'wedgering'
                        obj.trackerflag = 'PWR';
                    case 'VTF'
                        obj.trackerflag = 'VTF';
                end
                
                obj.tracker = Tracker(input.using_tracker, ...
                    sprintf('%02d%02d%s.edf', input.subject, obj.run_id, obj.trackerflag), obj.window);
                
                obj.input_handler = InputHandler(input.responder);
                                
                % listener will update properties of stimulus (e.g., contrast
                % or orientation) when the trial increments
                addlistener(obj, 'trial', 'PostSet',...
                    @(src,event)obj.update_stimulus(src,event) );
                
            end
            
        end
        
        % NOTE: although super convenient to be able to auto delete this
        % object when it's done, making sure to save it and the data, this
        % could easily bite you! It must be the case that the
        % filenameprefix property is only assigned to a value that won't
        % overwrite important files!
        function delete(obj)
            if ~isempty(obj.filenameprefix)
                obj.exp_end = GetSecs;
                
                writetable(as_table(obj), [obj.filenameprefix,'_meta.csv']);
                writetable(finalize_data(obj), [obj.filenameprefix,'_data.csv']);
                writetable(struct2table(struct(obj.stimulus),'AsArray',true),...
                    [obj.filenameprefix,'_stimulus.csv']);
                writetable(struct2table(struct(obj.window),'AsArray',true),...
                    [obj.filenameprefix,'_window.csv']);
                writetable(struct2table(struct(obj.input),'AsArray',true),...
                    [obj.filenameprefix,'_input.csv']);
                
            end
            
            % turn off warning after warned about events have occurred (in
            % the preceeding if statement)
            warning('off', 'MATLAB:structOnObject');

        end
        
        out = finalize_data(obj)
        out = as_table(obj)
        filenamechecker(obj)
        log_stim_on(obj, src, event)
        log_stim_off(obj, src, event)
        
        run(obj)
    end
    
    methods (Abstract)
        update_stimulus(obj, ~, ~)
        vbl = run_warmup(obj, vbl)
    end
    
    methods (Access = private, Static = true)
        version = get_version_string(fname, fallback)
    end
end

