function filenamechecker(obj)

% possible filenameprefix to test out

filenameprefix = sprintf('sub-%02d_task-%s_run-%02d', obj.subject, obj.exp, obj.run_id);

listing = dir(obj.savedir);

tmp = cellfun(@(x) any(strfind(filenameprefix,x)), {listing.name}, 'UniformOutput',false );

if any(arrayfun(@(x) x{1}==true, tmp))
    error('---files already exist with that prefix---');
else
    obj.filenameprefix = fullfile(obj.savedir, filenameprefix);
end

end

