function out = finalize_data(obj)
out = obj.data;
out.onset = out.trial_start - obj.trigger_sent;
out.duration = out.trial_end - out.trial_start;
end
