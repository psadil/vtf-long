function run(obj)

obj.tracker.startup();

% prepare grating stimulus for drawing
prepare(obj.stimulus, obj.window);

% draw cue indicating that we're ready for scanner start
draw_fixation(obj.stimulus, obj.window);
% DrawFormattedText(obj.window.pointer, 'Waiting for scanner start');
Screen('Flip', obj.window.pointer, GetSecs + 0.5);

% wait for the scanner trigger. vbl used to start syncing of first stim
% presentation
obj.trigger_sent = obj.input_handler.wait_for_start();
draw_fixation(obj.stimulus, obj.window);
% really want the screen to flip exactly when the trigger was sent. That is
% not going to happen (the moment has already passed). Instead, just add an
% arbitrary 500ms offset. That should be plenty of time for the drawing to
% finish. The mismatch between when the trigger was sent and when the first
% flip should have occurred is accounted for in run_warmup.
% note that this slack is somewhat tuned to make the first flip of the VTF
% task likely to happen at exactly 5 seconds (current warmup)
obj.stim_start = Screen('Flip', obj.window.pointer, obj.trigger_sent + 0.2);
vbl = obj.run_warmup(obj.stim_start);

% main experimental loop
for trial = 1:obj.n_trial
    obj.trial = trial;
    
    obj.input_handler.prepare(obj.input_handler.keys.task);
    
    switch obj.exp
        case 'VTF'
            vbl = run(obj.stimulus, obj.window, obj.data.n_stim_flip(trial), ...
                'vbl', vbl, 'deviant_flip', obj.data.freq_change_flip(trial), ...
                'input_handler', @()obj.input_handler.check_keys());
            obj.input_handler.shutdown();
            vbl = isi(obj.stimulus, obj.window, obj.data.n_isi_flip(trial), 'vbl', vbl);
        otherwise
            vbl = run(obj.stimulus, obj.window, obj.data.n_stim_flip(trial), ...
                'vbl', vbl, 'input_handler', @()obj.input_handler.check_keys());
            vbl = isi(obj.stimulus, obj.window, obj.data.n_isi_flip(trial), ...
                'vbl', vbl, 'input_handler', @()obj.input_handler.check_keys());
            obj.input_handler.shutdown();
    end
end
% data structured such that final ISI includes ending blank period (if
% present). Still need one final flip so that the final isi also lasts the
% full duration
obj.stim_end = Screen('Flip', obj.window.pointer, vbl + obj.stimulus.flip_after_sec);

% one extra flip so that there is definitely a blank screen during last TR 
Screen('Flip', obj.window.pointer, obj.stim_end + 1);

end
