function log_stim_off(obj, ~, ~)
obj.tracker.eyelink('StopRecording');
obj.data.trial_end(obj.trial) = obj.stimulus.stim_off_vbl;
WaitSecs(0.001);
obj.tracker.eyelink('Message', 'TRIAL_RESULT 0');
end
