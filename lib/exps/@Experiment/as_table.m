function out = as_table(obj)
out = table();
out.savedir = obj.savedir;
out.subject = obj.subject;
out.run_id =  obj.run_id;

out.seed = obj.seed;
out.exp_end = obj.exp_end;
out.exp_start = obj.exp_start;
out.trigger_sent = obj.trigger_sent;
out.stim_start = obj.stim_start;
out.stim_end = obj.stim_end;

out.datetaken = obj.datetaken;
out.version = obj.version;
end