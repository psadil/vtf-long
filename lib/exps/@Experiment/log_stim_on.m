function log_stim_on(obj, ~, ~)
obj.tracker.eyelink('message', 'SYNCTIME');
obj.data.trial_start(obj.trial) = obj.stimulus.first_stim_on_vbl;
end
