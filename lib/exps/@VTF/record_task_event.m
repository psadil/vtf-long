function record_task_event(obj,~,~)
obj.tracker.eyelink('Message', 'spatial frequency changed');
obj.data.freq_change_vbl(obj.trial) = obj.stimulus.vbl_of_event;
end
