classdef VTF < Experiment
    
    properties
        exp = 'VTF'
        filenameprefix
        data
        n_trial
        
        stimulus
        
        % how long to wait before starting stimuli (final ISI covers how
        % long to wait at end of experiment)
        warmup_sec = 5
        % 490: warmup + (8 orientations * 2 contrasts * 2 reps * 5 secs on) +
        % (8 orientations * 2 contrasts * 2 reps)*10 sec
        % isi + 5 second final blank screen
        % even though there is no final 'isi,' the last isi will be used in
        % full
        total_sec = 5 + 8*2*2*5 + (8*2*2)*10 + 5
    end
    
    
    methods
        function obj = VTF(input)
            obj@Experiment(input);
            
            obj.filenamechecker();
            
            obj.input_handler = InputHandler(input.responder);
            
            obj.stimulus = Grating();
            
            obj.data = prepare_data(obj);
            obj.n_trial = size(obj.data,1);
            
            % listen for responses provided, which is used to update data
            addlistener(obj.input_handler, 'press_times', 'PostSet',...
                @(src,evnt)obj.record_response(src,evnt) );
            
            % listen for event occurance, updating data when it happens
            addlistener(obj.stimulus, 'vbl_of_event', 'PostSet',...
                @(src,evnt)obj.record_task_event(src,evnt) );
            
            % listen for these changes to update relevant columns in data
            addlistener(obj.stimulus, 'first_stim_on_vbl', 'PostSet',...
                @(src,event)obj.log_stim_on(src,event) );
            addlistener(obj.stimulus, 'stim_off_vbl', 'PostSet',...
                @(src,event)obj.log_stim_off(src,event) );
        end
        
        function delete(obj)
            obj.stimulus.donut = [];
            obj.stimulus.mask = [];
            obj.stimulus.tex = [];
            obj.stimulus.offscreenwindow = [];
        end
        
        % methods defined in external files
        record_response(obj, ~, ~)
        record_task_event(obj,~,~)
        update_stimulus(obj, ~, ~)
        data = prepare_data(obj)
        vbl = run_warmup(obj, vbl)
    end
end

