function update_stimulus(obj, ~, ~)
obj.tracker.eyelink('Message', 'TRIALID %d', obj.trial);
obj.tracker.eyelink('Command', 'record_status_message "TRIAL %d"', obj.trial);

% note that we should not need to wait to start recording,
% given that the stimulus will always be drawn a bit later
% (determined by how often phase changes occur)
obj.tracker.eyelink('StartRecording');
obj.stimulus.contrast = obj.data.contrast(obj.trial);
obj.stimulus.orientation = obj.data.orientation(obj.trial);
obj.stimulus.which_deviant = obj.data.freq_change_feature(obj.trial);
end
