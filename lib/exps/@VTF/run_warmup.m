function vbl = run_warmup(obj, vbl)

warmup_end_after = (obj.warmup_sec - (obj.stim_start - obj.trigger_sent)) - 1;
n_warmup_flips = ceil(warmup_end_after*obj.stimulus.hz);

vbl = isi(obj.stimulus, obj.window, n_warmup_flips, ...
    'vbl', vbl, 'stim_off_vbl', false, 'input_handler', @()obj.input_handler.check_keys());

% final correction before trials actually start
% take the current time, and add the amount of time left over in the warmup
% period, minus flip_after_sec to account for the delay that will happen on
% the next flip
draw_fixation(obj.stimulus, obj.window);
vbl = Screen('flip', obj.window.pointer, vbl + (obj.warmup_sec - (vbl - obj.trigger_sent) - 1/obj.stimulus.hz));


end

