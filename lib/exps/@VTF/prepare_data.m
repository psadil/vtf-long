function data = prepare_data(obj)

data = readtable(fullfile(obj.input.root, 'task-vtf_blocking.csv'));
data = data(data.subject == obj.subject, :);
data = data(data.run == obj.run_id, :);
data.response = cell(size(data,1), 1);

end

