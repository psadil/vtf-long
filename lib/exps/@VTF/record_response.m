function record_response(obj, ~, ~)
% set to NaN by default, meaning this will only accept first
% response of trial
if isnan(obj.data.rt(obj.trial))
    obj.data.response(obj.trial) = {KbName(obj.input_handler.keys_pressed)};
    obj.data.rt(obj.trial) = ...
        max(obj.input_handler.press_times) - obj.stimulus.vbl_of_event;
end
end
