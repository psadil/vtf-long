function draw_fixation_grid(obj, window)

[sourceFactorOld, destinationFactorOld] = ...
    Screen('BlendFunction', window.pointer,'GL_SRC_ALPHA','GL_ONE_MINUS_SRC_ALPHA');
Screen('DrawTexture', window.pointer, obj.fixation_grid_tex,[],[],[]);
Screen('BlendFunction', window.pointer,sourceFactorOld,destinationFactorOld);

% % spokes
% Screen('DrawLines', window.pointer, obj.xy, obj.thickness, obj.color, [], 2);
% 
% % rings
% eccentricities_pix = (obj.radius_pix*2)*obj.slope.^(0:(obj.numrings-1));
% for ring = 1:obj.numrings
%     Screen('FrameArc', window.pointer, obj.color,...
%         CenterRect([0,0,eccentricities_pix(ring),eccentricities_pix(ring)], window.winRect),...
%         0,360, obj.thickness, obj.thickness);
% end

end

