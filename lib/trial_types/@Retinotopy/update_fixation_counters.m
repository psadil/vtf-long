function update_fixation_counters(obj,~,~)

% set next time fixation will change
obj.fixation_changes_sec = randi([ obj.meanchange_flip - obj.changeplusminus_flip, ...
    obj.meanchange_flip + obj.changeplusminus_flip], [1, 1])*(1/obj.hz);

end

