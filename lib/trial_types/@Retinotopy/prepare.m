function prepare(obj, window)

% bunches of conversions from size in degrees to size in pixels
obj.radius_pix = window.deg2pix(obj.radius_deg);
obj.fixation_pix = window.deg2pix(obj.fixation_deg);

% ------ fixation grid material ------

% want the rings to be anti-aliased, but multisampling on every flip
% demands too many resources. so, we instead draw into an anti-aliased
% window, then copy the contents of the anti-aliased window into another
% offscreen window. the final offscreen window will contain the fixation
% grid texture. see help ScreenOffscreenWindow for this idea
% sort of a pain to draw an entire, window-sized texture when only need the
% lines, but the performance is definitely way worse when drawing with
% multi-sampled display online
antialiasedWindowhandle = Screen('OpenOffscreenWindow', window.pointer, [.5,.5,.5,0], [], 128, 2, 64);
obj.fixation_grid_tex = Screen('OpenOffscreenWindow', window.pointer, [.5,.5,.5,0], [], 128, 1, []);
Screen('BlendFunction', antialiasedWindowhandle, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

angs = linspacecircular(0, pi, obj.numspokes);
obj.xy = [];
[toX, toY] = pol2cart(angs*2, obj.radius_pix);
for spoke = 1:obj.numspokes
    obj.xy = [obj.xy, [[window.xCenter; window.yCenter], [window.xCenter+toX(spoke); window.yCenter+toY(spoke)]]];
end

% spokes
Screen('DrawLines', antialiasedWindowhandle, obj.xy, obj.thickness, obj.color, [], 2);

% rings
eccentricities_pix = (obj.radius_pix*2)*obj.slope.^(0:(obj.numrings-1));
for ring = 1:obj.numrings
    Screen('FrameArc', antialiasedWindowhandle, obj.color,...
        CenterRect([0,0,eccentricities_pix(ring),eccentricities_pix(ring)], window.winRect),...
        0,360, obj.thickness, obj.thickness);
end

% now fixation_grid_tex contains the material
Screen('CopyWindow', antialiasedWindowhandle, obj.fixation_grid_tex);

% -------------

% bar is drawn on an offscreen window, which can be rotated without trouble
% to give the differently oriented bars
obj.outer_rect = CenterRect([0,0, obj.radius_pix*2, obj.radius_pix*2], window.winRect);

obj.circ_tex = Screen('OpenOffscreenWindow', window.pointer, ...
    [repelem(window.background,3), 0], obj.outer_rect, 128, 2);
Screen('FillOval', obj.circ_tex, [repelem(window.background,3), 0.5]);

% defines the amount of seconds that pass between flips
obj.flip_after_sec = ((window.framerate / obj.hz) - window.slack) * window.ifi;

load stimuli/images.mat images;
img_mats = images{1};
img_cells = cell(1,100);
img_full_pix = size(img_mats,1); % 1080
img_crop_pix = floor(obj.radius_pix*2)-4; % 1074
if mod(img_crop_pix,2)
    img_crop_pix = img_crop_pix - 1;
end
half_crop = (img_full_pix - img_crop_pix)/2;
for i = 1:100
    img_cells(i) = {img_mats((half_crop+1):(end-half_crop),(half_crop+1):(end-half_crop),:,i)};
end
obj.stimulus_tex = cellfun(@(x) Screen('MakeTexture', window.pointer, x), img_cells);

obj.fixation_changes_sec = randi([ obj.meanchange_flip - obj.changeplusminus_flip, ...
    obj.meanchange_flip + obj.changeplusminus_flip], [1, 1])*(1/obj.hz);


end