function vbl = run(obj, window, n_flips, varargin)
ip = inputParser;
addParameter(ip, 'vbl', GetSecs, @isnumeric);
addParameter(ip, 'input_handler', @obj.do_nothing);
% which stimuli to show on which flips
addParameter(ip, 'stimulus_imgs', randsample(100,n_flips,true));

parse(ip,varargin{:});
input = ip.Results;
vbl = input.vbl;

fixes = 1:size(obj.fixation_rgba, 2);
% need this switch to avoid double changes to fixation color
just_changed = false;

obj.init_mask(window);

for flip = 1:n_flips
    
    % draw masking region
    obj.draw_mask(window);
    
    % draw image, new image every flip
    Screen('BlendFunction', window.pointer, 'GL_DST_ALPHA', 'GL_ONE_MINUS_DST_ALPHA',[1,1,1,1]);
    Screen('DrawTexture', window.pointer, obj.stimulus_tex(input.stimulus_imgs(flip)));
    Screen('BlendFunction', window.pointer, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
    
    % draw special overlay
    obj.draw_fixation_grid(window);
    
    % Draw fixation
    Screen('DrawDots', window.pointer, [window.xCenter, window.yCenter],...
        obj.fixation_pix, obj.fixation_rgba(:,obj.which_fix), [], 2);
    
    % there are a few non-drawing tasks in this loop, but drawing is
    % finished at this point. Let PTB know it can go ahead with preparing
    % the stimulus for display
    Screen('DrawingFinished', window.pointer);
    
    % change position every flip. This has been calculated such that the
    % first flip >= this input is on the screen, and the bar will remain on
    % the screen until flip==obj.flips_on_screen. All flips after that will
    % have the bar off the screen
    obj.update_mask();
    
    % assign that stimulus started based on last flip (happens on the first
    % flip for which there is a 'last' flip)
    if flip == 2
        obj.first_stim_on_vbl = vbl;
    end
    
    fix_when = obj.fixation_changes_sec + obj.fixation_change_when;
    if ((vbl + (1/obj.hz)) >= fix_when) && ~just_changed
        % sample new, different fixation
        obj.which_fix = Sample(fixes(fixes~=obj.which_fix));
        just_changed = true;
    end
    if vbl >= fix_when
        % sample new, different fixation
        obj.fixation_change_when = vbl;
        just_changed = false;
    end
    
    % check for response (if trial called with input_handler)
    input.input_handler();
    
    % flip stimulus to screen
    vbl = Screen('Flip', window.pointer, vbl + obj.flip_after_sec);
    
end

end