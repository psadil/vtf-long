function draw_fixation(obj, window)

% draw special overlay
obj.draw_fixation_grid(window);

% Draw fixation
Screen('DrawDots', window.pointer, [window.xCenter, window.yCenter],...
    obj.fixation_pix, obj.fixation_rgba(:,obj.which_fix), [], 2);


end