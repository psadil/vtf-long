classdef Retinotopy < Stimulus
    
    properties (Constant)
        hz = 15
        radius_deg = 8 % this is radius of image to display (e.g., height of screen)
        
        % fixation stuff
        fixation_deg = 0.3
        fixation_rgba = [1 0 0 .5; 0 0 0 .5; 1 1 1 .5]'
        meanchange_sec = 3            % dot changes occur with this average interval
        changeplusminus_sec = 2       % plus or minus this amount
        meanchange_flip = Retinotopy.meanchange_sec*Retinotopy.hz
        changeplusminus_flip = Retinotopy.changeplusminus_sec*Retinotopy.hz
        
        % fixation grid material
        numspokes = 8
        numrings = 5
        thickness = 1
        color = 0.83 % .1765
        slope = 1/2
    end
    
    properties
        % window properties
        flip_after_sec
        
        % define rect of main circle to gray out
        outer_rect
        radius_pix
        circ_tex
        
        % will hold pre-made textures of all retinotopy images
        stimulus_tex
        
        % fixation grid material
        fixation_grid_tex
        fixation_pix 
        xy        
        % when should the next fixation change occur? recorded in seconds
        % since the last fixation change
        fixation_changes_sec        
        % initial fixation color
        which_fix = 3
        
        % trial-by-trial
        % draw ring? (else draw wedge). will be 'bar' for multibar
        wedgeorring
        % CW: clockwise (wedge only)
        % CCW: counter-clockwise (wedge only)
        % expand: rings move from center to outer (ring only)
        % contract: rings move from outer to center (ring only)
        % more complex for multibar
        direction        
    end
    
    properties (SetObservable, AbortSet)
        % when did the last fixation change occur? recorded in system time
        fixation_change_when = 0
    end
    
    methods
        function obj = Retinotopy()
            obj@Stimulus();
            
            % listen for event occurance, updating data when it happens
            addlistener(obj, 'fixation_change_when', 'PostSet',...
                @(src,evnt)obj.update_fixation_counters(src,evnt) );
            
        end
        draw_fixation_grid(obj, window)
        draw_fixation(obj, window)
        update_fixation_counters(obj,~,~)
        prepare(obj, window)
        vbl = run(obj, window, n_flips, varargin)
        vbl = isi(obj, window, n_flips, varargin)
    end
    
    methods (Abstract)
        init_mask(obj, window);
        draw_mask(obj, mask, window)
        update_mask(obj, mask)
    end
        
end

