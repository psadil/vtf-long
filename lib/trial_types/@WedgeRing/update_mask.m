function update_mask(obj, ~)

switch obj.wedgeorring
    case 'ring'
        obj.which_ring = obj.which_ring + obj.ring_transition;
        % need to prevent the which_ring index to ask for a value beyond
        % the precomputed rectangles. This is handled here by forcing the
        % last ring (defined by direction) to keep showing until end of
        % trial
        switch obj.direction
            case 'expand'
                obj.which_ring = min(obj.which_ring, size(obj.ring_outer_rect,2));
            case 'contract'
                obj.which_ring = max(obj.which_ring, 1);
        end
    case 'wedge'
        switch obj.direction
            case 'CCW'
                obj.wedge_start_ang = obj.wedge_start_ang - obj.wedge_transition_ang;
            case 'CW'
                obj.wedge_start_ang = obj.wedge_start_ang + obj.wedge_transition_ang;
        end
end

end

