function prepare(obj, window)

% common preparations
prepare@Retinotopy(obj,window);

% unsure where these eccentricities came from, but this is how the stimuli
% were created
% from prepareimages_retinotopy.m
eccs = readtable(fullfile('stimuli','eccs.csv'));
eccs = eccs.eccs';

framesperstep = 28*15/14;
newix = linspacefixeddiff(1, 1/30, 30+28*15);
eccsuse = interp1(1:length(eccs), eccs, newix, 'pchip');
numsteps = 28*15;

% make apertures
obj.ring_outer_rect = zeros(4,numsteps);
obj.ring_inner_rect = zeros(4,numsteps);
for pp=1:numsteps+30
    if pp<=30
        continue;
    end
    beginecc = max(1,pp - 3*framesperstep);  % the ring is 3 steps wide
    endecc = min(length(eccsuse),pp);
    
    % lower right side of rectangle given by x position of farthest point out
    % on circle (cos(0)=1). *2 because that position was only radius
    obj.ring_inner_rect(:,pp-30) = CenterRect([0, 0, ...
        eccsuse(beginecc)*obj.radius_pix*2, eccsuse(beginecc)*obj.radius_pix*2], ...
        window.winRect);
    obj.ring_outer_rect(:,pp-30) = CenterRect([0, 0, ...
        min(eccsuse(endecc),1)*obj.radius_pix*2, min(eccsuse(endecc),1)*obj.radius_pix*2],...
        window.winRect);
end

end