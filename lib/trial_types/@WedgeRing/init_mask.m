function init_mask(obj, ~)

% 'wedgeorring' type of mask
% 'direction' expand or contract (ring), CCW or CW (wedge)
% -----------------------------------------------------------------------

switch obj.wedgeorring
    case 'ring'
        switch obj.direction
            case 'expand'
                obj.which_ring = 1;
                obj.ring_transition = 1;
            case 'contract'
                obj.which_ring = size(obj.ring_outer_rect,2);
                obj.ring_transition = -1;
        end
    case 'wedge'
        obj.wedge_start_ang = 90;
end

end

