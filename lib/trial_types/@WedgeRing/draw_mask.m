function draw_mask(obj,window)

% draw masking region
Screen('BlendFunction', window.pointer, 'GL_ONE', 'GL_ZERO',[0,0,0,1]);
Screen('FillRect', window.pointer, [repelem(window.background,3), 0]);

switch obj.wedgeorring
    case 'ring'
        Screen('FillOval', window.pointer, ...
            [repmat(window.background,3,2);[1,0]], [obj.ring_outer_rect(:,obj.which_ring), obj.ring_inner_rect(:,obj.which_ring)]);
        Screen('BlendFunction', window.pointer, 'GL_DST_ALPHA', 'GL_SRC_ALPHA',[0,0,0,1]);
        Screen('DrawTexture', window.pointer, obj.circ_tex);
    case 'wedge'
        Screen('FillArc', window.pointer, [repelem(window.background,3), 1],...
            obj.outer_rect, obj.wedge_start_ang, obj.wedge_arc_ang);
end

end

