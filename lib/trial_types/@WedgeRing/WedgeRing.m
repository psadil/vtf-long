classdef WedgeRing < Retinotopy
    
    properties (Constant)
        % angles of transitions per flip
        wedge_arc_ang = -90
    end
    
    properties
        % change trial-by-trial
        wedge_transition_ang
        
        % change flip-by-flip
        ring_outer_rect
        ring_inner_rect
        which_ring
        ring_transition
        wedge_start_ang
        
    end
    
    methods
        function obj = WedgeRing()
            obj@Retinotopy();
        end
        
        prepare(obj, window)
        mask = init_mask(obj, window)
        draw_mask(obj, window)
        update_mask(obj)
        
    end
        
end

