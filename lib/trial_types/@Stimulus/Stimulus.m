classdef Stimulus < handle
    
    properties (Abstract)
        flip_after_sec
    end
    
    properties (SetObservable, AbortSet)
        % experiment objects will listen for changes to these
        first_stim_on_vbl
        stim_off_vbl
        vbl_of_event = 0
    end
    
    methods
        function obj = Stimulus()
        end
        
    end
    
    methods (Abstract)
        draw_fixation(obj, window)        
        vbl = isi(obj, window, n_flips, vbl, varargin)
    end
    
    methods (Static)
        % do_nothing function comes up in a few places. E.g., input_handler
        % during run functions might call do_nothing when there is no
        % attached input_handler (as in the case of demos)
        function [] = do_nothing(varargin)
        end
    end
    
end

