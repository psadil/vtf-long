function vbl = isi(obj, window, n_flips, varargin)
% similar to run method for retinotopy, but no drawing of masks or stimuli

ip = inputParser;
addParameter(ip, 'vbl', GetSecs, @isnumeric);
addParameter(ip, 'input_handler', @obj.do_nothing);
addParameter(ip, 'stim_off_vbl', true, @islogical);

parse(ip,varargin{:});
input = ip.Results;
vbl = input.vbl;

for flip = 1:n_flips

    % Draw fixation
    Screen('DrawDots', window.pointer, [window.xCenter, window.yCenter],...
        obj.fixation_pix, obj.fixation_rgb, [], 2);
    
    Screen('DrawingFinished', window.pointer);    
    if flip == 2 && input.stim_off_vbl    
        obj.stim_off_vbl = vbl;
    end    
    % flip stimulus to screen
    vbl = Screen('Flip', window.pointer, vbl + obj.flip_after_sec);    
end
end