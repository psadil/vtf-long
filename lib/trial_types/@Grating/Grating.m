classdef Grating < Stimulus
    
    properties (Constant)
        hz = 5
        n_phases = 16
        
        width_pix = 2^12
        height_pix = 2^12
        backgroundColorOffset = [0.5 0.5 0.5 0]
        contrastPreMultiplicator = 0.5
        smooth_deg = 0 % smoothing of grating stimulus (not used in favor of conv)
        useAlpha = false
        smooth_method = 0
        smooth_size_deg = 1 %size of fspecial smoothing kernel
        smooth_sd_deg = .5 %smoothing kernel sd
        radius_outer_deg = 7 - Grating.smooth_size_deg/2
        radius_inner_deg = 1.5 + Grating.smooth_size_deg/2
        mask_deg = 7*2;
        freq_cpd = 2 % cycles per degree
        freq_cpd_deviant = [1, 3]
        fixation_deg = 0.2 % diameter of fixation (in Rademakers et al, fixation during mapping was this small)
        fixation_rgb = [0.7843; 0; 0.8886] % magenta
        
        use_alpha = true
    end
    
    properties
        tex
        tex_inner
        
        % to calculate based on window size
        radius_outer_pix
        radius_inner_pix
        freq_cpp % cycles per pixel
        freq_cpp_deviant
        smooth_pix
        smooth_size_pix 
        smooth_sd_pix
        mask_pix
        donut
        mask
        offscreenwindow
        
        convoperator
        kernel
        
        fixation_pix
        flip_after_sec
        
        % properties that can change trial-by-trial
        phase_deg = 0
        orientation = 0
        contrast = 1
        which_deviant = 1
                
    end
    
    
    methods
        function obj = Grating()
            obj@Stimulus();
        end
        
        vbl = prepare(obj, window)
        vbl = run(obj, window, n_flips, varargin)
        vbl = isi(obj, window, n_flips, varargin)
        draw_fixation(obj, window)

    end
        
end

