function vbl = run(obj, window, n_flips, varargin)
ip = inputParser;
addParameter(ip, 'vbl', GetSecs, @isnumeric);
addParameter(ip, 'deviant_flip', NaN, @isnumeric);
addParameter(ip, 'input_handler', @obj.do_nothing);
parse(ip,varargin{:});
input = ip.Results;
vbl = input.vbl;

for flip = 1:n_flips
    
    % Draw the grating. It's defined such that we don't want any alpha
    % blending going on
    [sourceFactorOld, destinationFactorOld] = Screen('BlendFunction', ...
        window.pointer, 'GL_ONE', 'GL_ZERO');
    if flip == input.deviant_flip
        Screen('DrawTexture', window.pointer, obj.tex, [], [],...
            obj.orientation, [], [], [], [], [],...
            [obj.phase_deg, obj.freq_cpp_deviant(obj.which_deviant), obj.contrast, 0]);
    else
        Screen('DrawTexture', window.pointer, obj.tex, [], [],...
            obj.orientation, [], [], [], [], [],...
            [obj.phase_deg, obj.freq_cpp, obj.contrast, 0]);
    end
    % return to original blending function
    Screen('BlendFunction', window.pointer, sourceFactorOld, destinationFactorOld);
    % draw mask to blur edges
    Screen('DrawTexture', window.pointer, obj.offscreenwindow);
    
    % Draw fixation
    Screen('DrawDots', window.pointer, [window.xCenter, window.yCenter],...
        obj.fixation_pix, obj.fixation_rgb, [], 2);
    
    % there are a few non-drawing tasks in this loop, but drawing is
    % finished at this point. Let PTB know it can go ahead with preparing
    % the stimulus for display
    Screen('DrawingFinished', window.pointer);
    
    % counter phase every flip
    obj.phase_deg = mod(obj.phase_deg+180, 360);
    
    % assign that stimulus started based on last flip (happens on the first
    % flip for which there is a 'last' flip)
    if flip == 2
        obj.first_stim_on_vbl = vbl;
    end
    
    % record the flip time of when the target event occurred
    if flip == (input.deviant_flip + 1)
        obj.vbl_of_event = vbl;
    end
    
    % check for response (if trial called with input_handler)
    input.input_handler();
    
    % flip stimulus to screen
    vbl = Screen('Flip', window.pointer, vbl + obj.flip_after_sec);
    
end

end