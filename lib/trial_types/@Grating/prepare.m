function vbl = prepare(obj, window)

% bunches of conversions from size in degrees to size in pixels
obj.radius_outer_pix = window.deg2pix(obj.radius_outer_deg);
obj.radius_inner_pix = window.deg2pix(obj.radius_inner_deg);
obj.freq_cpp = (1 / window.deg2pix(1/obj.freq_cpd));
obj.freq_cpp_deviant = (1 ./ window.deg2pix(1./obj.freq_cpd_deviant));
obj.smooth_pix = window.deg2pix(obj.smooth_deg);
obj.fixation_pix = window.deg2pix(obj.fixation_deg);
obj.smooth_size_pix = window.deg2pix(obj.smooth_size_deg);
obj.smooth_sd_pix = window.deg2pix(obj.smooth_sd_deg);
obj.mask_pix = window.deg2pix(obj.mask_deg);

% this is the main stimulus that will be drawn
obj.tex = CreateProceduralSineGrating(window.pointer, ...
    obj.width_pix, obj.height_pix, obj.backgroundColorOffset, obj.radius_outer_pix + 600, ...
    obj.contrastPreMultiplicator);

% however, the stimulus also has a mask, defined by a (guassian blurred)
% annulus
X = -0.5*obj.mask_pix+.5:1:.5*obj.mask_pix-.5; 
Y = -0.5*obj.mask_pix+.5:1:.5*obj.mask_pix-.5;
[x,y] = meshgrid(X,Y);
donut_out = x.^2 + y.^2 <= (obj.radius_outer_pix)^2;
donut_in = x.^2 + y.^2 >= (obj.radius_inner_pix)^2;
donut = donut_out.*donut_in;
obj.donut = filter2(fspecial('gaussian', round(obj.smooth_size_pix), round(obj.smooth_sd_pix)), donut);
obj.donut = abs(obj.donut - 1);
obj.mask = Screen('MakeTexture', window.pointer, cat(3,ones(size(obj.donut))*.5,obj.donut));

% mask will be done as offscreen window
% to check, try imshow(obj.donut)
obj.offscreenwindow = Screen('OpenOffscreenWindow', window.pointer, window.background);
Screen('DrawTexture', obj.offscreenwindow, obj.mask);

% Draw the grating once, just to make sure the gfx-hardware is ready, and
% the mex file loaded
Screen('DrawTexture', window.pointer, obj.tex, [], [],...
    obj.orientation, [], [], [], [], [], [obj.phase_deg, obj.freq_cpp, 0, 0]);

% defines the amount of seconds that pass between flips
obj.flip_after_sec = ((window.framerate / obj.hz) - window.slack) * window.ifi;

% Perform initial flip to prepare grating
vbl = Screen('Flip', window.pointer, GetSecs + obj.flip_after_sec);

end