function draw_mask(obj, window)

Screen('BlendFunction', window.pointer, 'GL_ONE', 'GL_ZERO',[0,0,0,1]);
Screen('FillRect', window.pointer, [repelem(window.background,3), 0]);
Screen('DrawTexture', window.pointer, obj.bar_tex, [], ...
    OffsetRect(obj.bar_rect, obj.x_offset, obj.y_offset), -obj.direction);
Screen('BlendFunction', window.pointer, 'GL_DST_ALPHA', 'GL_SRC_ALPHA',[0,0,0,1]);
Screen('DrawTexture', window.pointer, obj.circ_tex);


end

