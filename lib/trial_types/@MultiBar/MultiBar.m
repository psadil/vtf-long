classdef MultiBar < Retinotopy
    
    properties
        % moving position of bar rectangle
        bar_transition_pix
        bar_width_pix
        bar_tex
        % properties that change trial-by-trial
        direction_rad
        direction_x
        direction_y
        ori_rad_offset
        x_offset
        y_offset
        % properties that can change flip-by-flip
        bar_rect        
    end
    
    methods
        function obj = MultiBar()
            obj@Retinotopy();
        end        
        
        function delete(obj)
            obj.bar_tex = [];
        end

        prepare(obj, window)
        init_mask(obj, window);
        draw_mask(obj, mask, window)
        update_mask(obj, mask)
    end
    
end

