function init_mask(obj, window)

% 'direction' is direction of motion
% 'orientation' is orientation of bar (set by direction)
% 'rotation' is how much to rotate the vertically orientated bar
% 'offset' is how much the center of the bar moves at the start
% direction => path          => orientation => rotation => offset, (x,y)
% -----------------------------------------------------------------------
% 0         => Left -> Right => vertical    => 0        => (-, 0)
% 45        => LL -> UR      => left tilt   => -45      => (-, +)
% 90        => Bottom -> Top => horizontal  => -90      => (0, +)
% 135       => LR -> UL      => right tilt  => -135     => (-, +)
% 180       => Right -> Left => vertical    => -180     => (+, 0)
% 225       => UR -> LL      => left tilt   => -225     => (+, -)
% 270       => Top -> Bottom => horizontal  => -270     => (0, +)
% 315       => UL -> LR      => right tilt  => -315     => (-, -)

% recenter rectangle for next trial
obj.bar_rect = CenterRectOnPoint([0, 0, obj.bar_width_pix, (obj.radius_pix*2)], ...
    window.xCenter, window.yCenter);

% used in defining the transition steps
obj.direction_rad = deg2rad(obj.direction);
obj.direction_x = cos(obj.direction_rad)*obj.bar_transition_pix;
% direction is -1 because up is negative in screen space
obj.direction_y = -1*sin(obj.direction_rad)*obj.bar_transition_pix;

% used to define the starting position offset
obj.ori_rad_offset = deg2rad(-obj.direction);
% +3 is so that the stimulus does not start out inside the mask
% anti-aliasing makes the bounds not quite precise
obj.x_offset = -1*cos(obj.ori_rad_offset)*(obj.radius_pix + obj.bar_width_pix/2 + 3);
obj.y_offset = -1*sin(obj.ori_rad_offset)*(obj.radius_pix + obj.bar_width_pix/2 + 3);

end

