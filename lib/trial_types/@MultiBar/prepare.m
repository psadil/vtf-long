function prepare(obj, window)

% common preparations
prepare@Retinotopy(obj,window);

obj.bar_width_pix = (1/8) * obj.radius_pix*2; 

% all bars are on screen for 28 seconds
% extra bar_width_pix because bar starts just outside of main circle
% +3, see init_mask. extra slack for pushing bar outside of anti-aliased
% lines at very start of trials
obj.bar_transition_pix = (obj.radius_pix*2 + obj.bar_width_pix  + 3) / (28*obj.hz);

obj.bar_tex = Screen('OpenOffscreenWindow', window.pointer, [], ...
    [0, 0, obj.bar_width_pix, (obj.radius_pix*2)], 128);

% rectangle 'starts'
obj.bar_rect = CenterRectOnPoint([0, 0, obj.bar_width_pix, (obj.radius_pix*2)], window.xCenter, window.yCenter);

end