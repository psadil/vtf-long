function  main(varargin)


%% collect input
ip = inputParser;
addParameter(ip, 'subject', 99, @isnumeric);
addParameter(ip, 'responder', 'user', @(x) sum(strcmp(x, {'user'}))==1);
addParameter(ip, 'fMRI', true, @islogical);
addParameter(ip, 'debuglevel', 0, @(x) x == 1 | x == 10 | x == 0);
addParameter(ip, 'skipsynctests', 0, @(x) any(x == 0:2));
addParameter(ip, 'using_tracker', true, @islogical);
addParameter(ip, 'run_id', 0, @isnumeric);
addParameter(ip, 'root', pwd, @ischar);
addParameter(ip, 'exp', 'VTF', @(x) sum(strcmp(x, {'VTF','wedgering','multibar','calibrate'}))==1);
addParameter(ip, 'gamma', true, @islogical);
parse(ip,varargin{:});
input = ip.Results;
addpath(genpath('lib'));

% gather demographics for practice run
if input.run_id == 0 && strcmp(input.responder,'user') && input.debuglevel == 0
    demographics('events');
end


%%  setup
PsychDefaultSetup(2);

% setup expt and run
switch input.exp
    case 'VTF'
        exp = VTF(input);
    case 'multibar'
        exp = PRF(input);
    case 'wedgering'
        exp = PRF(input);
    case 'calibrate'
        exp = Calibrate(input);
end
exp.run();

%% save data

% delete to force save
delete(exp);
rmpath(genpath('lib'));


end