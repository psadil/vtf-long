
diary sub-01_ses-1_diary.txt
sub = 1;

main('subject', sub, 'run', 1, 'exp', 'calibrate') 

% starts with 
% 1) Fieldmapping scan (required to begin for aligning of scan slices)
% 2) anatomical scan

main('subject', sub, 'run', 2, 'exp', 'calibrate') 

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 1, 'exp', 'multibar') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 1, 'exp', 'VTF', 'using_tracker', false)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 2, 'exp', 'VTF')

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 3, 'exp', 'VTF')

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 1, 'exp', 'wedgering') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 4, 'exp', 'VTF') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 5, 'exp', 'VTF') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 6, 'exp', 'VTF') 

diary off
