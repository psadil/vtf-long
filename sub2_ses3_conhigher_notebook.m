
diary sub-02_ses-3_diary.txt
sub = 2;

main('subject', sub, 'run', 5, 'exp', 'calibrate') 

% starts with 
% 1) anatomical scan
% 1.5) Aligning of scans to calcarine
% 2) Fieldmapping scan 

main('subject', sub, 'run', 6, 'exp', 'calibrate') 

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 3, 'exp', 'multibar','gamma',false) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 13, 'exp', 'VTF')

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 14, 'exp', 'VTF')

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 15, 'exp', 'VTF')

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 3, 'exp', 'wedgering','gamma',false) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 16, 'exp', 'VTF') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 17, 'exp', 'VTF') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 18, 'exp', 'VTF') 

diary off
