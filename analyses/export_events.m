function export_events(sub, runs, ses, run_offset)

for run = runs
    data = readtable(fullfile('events', sprintf('sub-%02d_task-VTF_run-%02d_data.csv', sub, run)));
    data.freq_change_feature = [];
    data.n_isi_sec = [];
    data.n_stim_sec = [];
    data.n_stim_flip = [];
    data.n_isi_flip = [];
    data.freq_change_flip = [];
    data.freq_change_vbl = [];
    data.trial_start = [];
    data.trial_end = [];
    data.rt = [];
    data.response = [];
    data.trial = [];
    
    if sub==1 && ses == 1 && any(run== 1:6)
        data.orientation = mod(data.orientation,180);
    end
    data.orientation = round(data.orientation,4);
    
    data.onset = round(data.onset,4);
    data.duration = round(data.duration,4);
    
    trial_type = cell(size(data,1),1);
    for row = 1:size(data,1)
        trial_type{row} = sprintf('con-%g_ori-%g', data.contrast(row), data.orientation(row));
    end
    data.trial_type = trial_type;
    
    writetable(data, fullfile('events', ...
        sprintf('sub-%02d_ses-%d_task-con_run-%02d_events.tsv', sub, ses, run - run_offset)),...
        'Delimiter','tab', 'FileType', 'text');
end

end

