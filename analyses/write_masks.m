function write_masks(type)

% number of frames in written movie (matches number of fmri scans)
nscans = 300;

% resolution (both dims) of movie output
outres = 200;

% across how many frames should downsampled mask average?
movingwinavg = 15;

switch type
    case 'multibar'
        expt = 93;
    case 'wedgering'
        expt = 94;
end

% using first sub's mask image because they should all be the same
load(fullfile('events',sprintf('sub-01_run-01_task-%d.mat', expt)),...
    'frameorder');
load(fullfile('events',sprintf('sub-01_run-01_task-%d_maskimages.mat', expt)),...
    'maskimages');

maskres = size(maskimages,1);
framesinorig = size(frameorder,2);

stimulus = zeros(maskres,maskres,framesinorig,'uint8');

for stim = 1:framesinorig
    if frameorder(2,stim) ~= 0
       stimulus(:,:,stim) = maskimages(:,:,frameorder(2,stim)); 
    end
end

% downsample stimuli to resolution of scans (1 stim for each scan image)
avgd = zeros(maskres,maskres,nscans,'uint8');
for i = 1:maskres
    for j = 1:maskres
        tmp = stimulus(i,j,:);
        avgd(i,j,:) = mean(reshape(tmp,movingwinavg,[]));
    end    
end
    
% downsample resolution of image
smaller = imresize(avgd,'OutputSize',[outres, outres]);

v = VideoWriter(fullfile('stimuli',sprintf('task-%s.avi',type)),'Grayscale AVI');
open(v);
for frame = 1:nscans
   writeVideo(v,smaller(:,:,frame)); 
end
close(v)

end

