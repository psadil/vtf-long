#!/bin/bash
# requires eyelink developers kit
# https://www.sr-support.com/forum/downloads/eyelink-display-software/39-eyelink-developers-kit-for-windows-windows-display-software

for sub in {1..4}; do
  for filename in events/0${sub}*VTF.edf; do
    edf2asc.exe $filename
  done
  
  run0=1;
  for filename in events/0${sub}*VTF.asc; do
    if (($run0 < 7)); 
    then
       ses=1
    elif (($run0 >= 7 & $run0 < 13));
    then
      ses=2
    else
      ses=3
    fi
    let "run = run0 - 6*(ses-1)"
    cp $filename events/sub-0${sub}_ses-${ses}_task-con_run-0${run}_eyetrack.asc
    ((run0++))
  done


  # export pRF, wedgering
  for filename in events/0${sub}*PWR.edf; do
    edf2asc.exe $filename
  done

  run0=0;
  for filename in events/0${sub}*PWR.asc; do
    ((run0++))
    # false start, this run not real
    if (($sub==2 & run0==3));
    then
      continue
    fi
    run=1
    if (($run0 < 2)); 
    then
       ses=1
    elif (($run0 >= 2 & $run0 < 3));
    then
      ses=2
    else
      ses=3
    fi

    cp $filename events/sub-0${sub}_ses-${ses}_task-wedgering_run-0${run}_eyetrack.asc
  done

  # export pRF, multibar
  for filename in events/0${sub}*PMB.edf; do
    edf2asc.exe $filename
  done

  run0=1;
  for filename in events/0${sub}*PMB.asc; do
    run=1
    if (($run0 < 2)); 
    then
       ses=1
    elif (($run0 >= 2 & $run0 < 3));
    then
      ses=2
    else
      ses=3
    fi

    cp $filename events/sub-0${sub}_ses-${ses}_task-multibar_run-0${run}_eyetrack.asc
    ((run0++))
  done
done
