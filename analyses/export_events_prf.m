function export_events_prf(sub, runs, ses, run_offset, type)

for run = runs
    data = readtable(fullfile('events', sprintf('sub-%02d_task-%s_run-%02d_data.csv', sub, type, run)));
    out = table();
    out.onset = data.onset;
    out.duration = data.duration;
    out.direction = data.direction;
    
    writetable(data, fullfile('events', ...
        sprintf('sub-%02d_ses-%d_task-%s_run-%02d_events.tsv', sub, ses,type, run - run_offset)),...
        'Delimiter','tab', 'FileType', 'text');
end

end

