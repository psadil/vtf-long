%% run from scanner 
diary sub-03_ses-3_diary.txt
sub = 3;

main('subject', sub, 'run', 4, 'exp', 'calibrate') 

% starts with 
% 1) anatomical scan
% 1.5) Aligning of scans to calcarine
% 2) Fieldmapping scan 

main('subject', sub, 'run', 5, 'exp', 'calibrate') 

% single-band reference scan (~24 seconds)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 3, 'exp', 'multibar', 'gamma', false) 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 13, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 14, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 15, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 3, 'exp', 'wedgering', 'gamma', false) 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 16, 'exp', 'VTF') 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 17, 'exp', 'VTF') 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 18, 'exp', 'VTF') 

diary off
