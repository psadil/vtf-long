
sub = 1;

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 20) 

% starts with 
% 1) calibration
% 2) Fieldmapping scan (required to begin for aligning of scan slices)
% 3) anatomical scan

% proceeds as follows

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 7) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 8) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 9) 

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 2, 'exp', 'multibar') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 10) 


% stopped here for bathroom brake. Tacking remaining scans on to session 3
% runs
