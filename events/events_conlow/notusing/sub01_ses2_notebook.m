% second feasibility session. Same as first session, except voxel
% resolution will be reduced from 2.2 mm^3 to 1.6mm^3 (and a few other
% parameters to make this possible).
%

sub = 1;

% session 2, so no pre-run scan

% scanning starts with 
% 1) eyetracker calibration 
main('subject', sub, 'run', 20) 
% 2) Fieldmapping scan (required to begin for aligning of scan
% slices)
% 3) anatomical scan

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 7) 

% task requires 425 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 8) 

% MUST run due to changing of color range;
sca;
clear all;

% retinotopy tasks require 5 minutes
% run 1, 93=multibar
runretinotopy_vtf('subnum',sub,'runnum',2,'expnum',93)

% MUST run due to changing of color range;
sca;
clear all;

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 9) 

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 10) 

% MUST run due to changing of color range;
sca;
clear all;

% run 2, 94=wedgeringmash, 5 minutes
runretinotopy_vtf('subnum', sub, 'runnum', 2, 'expnum', 94)

% MUST run due to changing of color range;
sca;
clear all;

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 11) 

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 12) 

% goal of 6 runs, would be nice to get 7

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 13) 

% task requires 435 seconds (7 minutes, 15 seconds)
main('subject', sub, 'run', 14) 
