sub = 1;

% task requires 425 seconds (7 minutes, 5 seconds)
% Run to give practice, plus collect demographics
main('subject', sub, 'run', 0, 'fMRI', false, 'using_tracker', false, 'skipsynctests', 2); 

% scanning starts with 
% 1) eyetracker calibration
% 2) Fieldmapping scan (required to begin for aligning of scan
% slices)
% 3) anatomical scan

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 1) 

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 2) 

% MUST run due to changing of color range;
sca;
clear all;

% retinotopy tasks require 5 minutes
% run 1, 93=multibar
runretinotopy_vtf('subnum',sub,'runnum',1,'expnum',93)

% MUST run due to changing of color range;
sca;
clear all;

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 3) 

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 4) 

% MUST run due to changing of color range;
sca;
clear all;

% run 2, 94=wedgeringmash, 5 minutes
runretinotopy_vtf('subnum', sub, 'runnum', 1, 'expnum',94)

% MUST run due to changing of color range;
sca;
clear all;

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 5) 

% task requires 425 seconds (7 minutes, 5 seconds)
main('subject', sub, 'run', 6) 

% completed 6 runs on first session