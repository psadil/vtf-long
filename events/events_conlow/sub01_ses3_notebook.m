
sub = 1;

% starts with 
% 1) calibration
% 2) Fieldmapping scan (required to begin for aligning of scan slices)
% 3) anatomical scan

% proceeds as follows

 % ---------- Scans not collected during session 2
% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 11) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 12) 


% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 3, 'exp', 'multibar') 

% note, wanted wedgering first, but ran multibar first

% ------------- beginning of regular session 3

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 2, 'exp', 'wedgering') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 13)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 14)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 15)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 3, 'exp', 'wedgering') 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 16) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 17) 

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 18) 

