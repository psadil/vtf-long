%% run from scanner 
diary sub-04_ses-2_diary.txt
sub = 4;

main('subject', sub, 'run', 2, 'exp', 'calibrate') 

% starts with 
% 1) anatomical scan
% 1.5) Aligning of scans to calcarine
% 2) Fieldmapping scan 

main('subject', sub, 'run', 3, 'exp', 'calibrate') 

% single-band reference scan (~24 seconds)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 2, 'exp', 'multibar', 'gamma', false) 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 7, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 8, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 9, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 2, 'exp', 'wedgering', 'gamma', false) 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 10, 'exp', 'VTF') 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 11, 'exp', 'VTF') 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 12, 'exp', 'VTF') 

diary off
