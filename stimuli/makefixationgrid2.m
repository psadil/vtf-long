function makefixationgrid2(numspokes,numrings,thickness,color, slope)

% function f = drawpolargrid(numspokes,numrings,thickness,color)
%
% <numspokes> is the positive number of spokes starting from 0 degrees (x+ axis)
% <numrings> is the number of rings
% <thickness> is the line thickness in points
% <color> is a 3-element vector or scalar with the line color (in 0-1)
% <slope> ratio controlling how much rings are separated
% Uses PTB
%
% example:
% makefixationgrid(8, 5, 1, .1765, 1/2)



%% open window
Screen('Preference', 'SkipSyncTests', 2);
PsychDefaultSetup(2);
PsychImaging('PrepareConfiguration');
PsychImaging('AddTask', 'General', 'FloatingPoint32BitIfPossible');
PsychImaging('AddTask', 'General', 'UseFastOffscreenWindows')

[pointer, winRect] = PsychImaging('OpenWindow', 0, 0,[],[],[],[],[]);

% need GLSL for procedural definition of gratings
AssertGLSL;

% Turn on blendfunction for antialiasing of drawing dots
Screen('BlendFunction', pointer, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

topPriorityLevel = MaxPriority(pointer);
Priority(topPriorityLevel);

% define some landmark locations to be used throughout
[xCenter, yCenter] = RectCenter(winRect);

offscreenpointer = Screen('OpenOffscreenWindow', pointer, 0.5, [], 128, [], 64);
offscreenpointer2 = Screen('OpenOffscreenWindow', pointer, 0.5, [], 128, [], []);
Screen('BlendFunction', offscreenpointer, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
% Screen('BlendFunction', offscreenpointer2, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
%% drawing

% spokes
angs = linspacecircular(0,pi,numspokes);
xy = [];
[toX,toY] = pol2cart(angs*2, winRect(4)/2);
for spoke = 1:numspokes
    xy = [xy, [[xCenter; yCenter], [xCenter+toX(spoke); yCenter+toY(spoke)]]];
end

Screen('DrawLines', offscreenpointer, xy, thickness, color, [], 2);


% rings
% eccentricities = 16*slope.^(0:(numrings-1));
% eccentricities_pix = deg2pix(eccentricities, winRect);
eccentricities_pix = 1080*slope.^(0:(numrings-1));
for ring = 1:numrings
    Screen('FrameArc',offscreenpointer,color,...
        CenterRect([0,0,eccentricities_pix(ring),eccentricities_pix(ring)],winRect),...
        0,360, thickness, thickness);
end

Screen('CopyWindow', offscreenpointer, offscreenpointer2);
Screen('DrawTexture', pointer, offscreenpointer2);

Screen('Flip', pointer);

% specialoverlay = Screen('GetImage', pointer, [], [], [], 4);
% specialoverlay(:,:,4) = specialoverlay(:,:,1);
% specialoverlay(:,:,1:3) = uint8(255);
% save('stimuli/fixationgrid.mat','specialoverlay');

WaitSecs(5);
sca;
end
