%% fit the gamma and spline models - importantly, fit the constant term for use in illuminance-normalization

numMeasures = 24;
intensity_vals = 0:1/(numMeasures-1):1;

T = readtable('stimuli/lx_hmrc.csv');
readings = table2array(T)';

displayGamma = zeros(1,3);
displayConstant = zeros(1,3);
gammaTableR = zeros(256,3);
displaySplineModel = zeros(256,3);

for channel = 1:3
    
    %Substract baseline (background) illuminance. Do not normalize.
    displayBaseline = min(readings(channel,:));
    chan_vals = (readings(channel,:) - displayBaseline);
    
    %Gamma function fitting
    fo = fitoptions('a*(x^g)','Lower',[0,1],'Upper',[400,3]);
    g = fittype('a*(x^g)','options',fo);
    fittedmodel = fit(intensity_vals',chan_vals',g);
    displayGamma(channel) = fittedmodel.g;
    displayConstant(channel) = fittedmodel.a;
    gammaTableR(:,channel) = ((([0:255]'/255))).^(1/fittedmodel.g); %#ok<NBRAK>
    
    firstFit = fittedmodel([0:255]/255); %#ok<NBRAK>
    
    %Spline interp fitting
    fittedmodel = fit(intensity_vals',chan_vals','splineinterp');
    displaySplineModel(:,channel) = fittedmodel([0:255]/255); %#ok<NBRAK>
    
    figure;
    plot(255*intensity_vals', chan_vals', '.', [0:255], firstFit, '--', [0:255], displaySplineModel(:,channel), '-.'); %#ok<NBRAK>
    legend('Measures', 'Gamma model', 'Spline interpolation');
    title(sprintf('Gamma model x^{%.2f} vs. Spline interpolation', displayGamma(channel)));
    
end

%invert gamma table to get predicted normalized readings
readings255 = 255*(displaySplineModel ./ repmat(max(displaySplineModel),[256,1]));
pred_norm_readings = zeros(256,3);
for ii = 1:256
[~, pred_norm_readings(ii,:)] = min(abs((ii-1) - readings255));
end
pred_norm_readings = (pred_norm_readings-1)./255;


% load('D:\git\fMRI\VTF-long\stimuli\gammaTable-cemnl-rgb.mat')
load('D:\git\fMRI\VTF-long\stimuli\gammaTable-hmrc-factory-rgb.mat')

figure
plot(pred_norm_readings)

figure
hold on
h(1) = plot(gammaTableR(:,1), 'red');
h(2) = plot(gammaTableR(:,2), 'green');
h(3) = plot(gammaTableR(:,3), 'blue');
h(4) = plot(gammaTable(:,1), '--red');
h(5) = plot(gammaTable(:,2), '--green');
h(6) = plot(gammaTable(:,3), '--blue');
legend(h([1, 4]), {'New','Original'})

%% save the models if you wish
% save(['gammaTable-',monitor,'-rgb'],'gammaTable')
% save(['gammaFit-',monitor],'displayGamma','displayConstant','readings','intensity_vals')

