
rng(02052019);

n_subject = 20;
n_orientation = 8;
final_blank_sec = 15;
n_run = 24;
n_stim_sec = 5;
stim_hz = 5;
isi_available = [9:0.4:11, 9.2:0.4:10.8, 8:0.2:12]'; % with 8 oris
% isi_available = [9:0.4:11, 8:0.4:12, 9:0.2:11]'; % with 7 oris

% remove just one 10 because final isi is forced to be 10 and is included
% in variable 'final_blank_sec'
tens = find(isi_available == 10);
isi_available(tens(1)) = [];
% note that mean(isi_available) == 10
T = table();

for subject = 1:n_subject
    
    for run = 0:n_run
        t = table();
        
        orientation = linspace(0,180-(180/n_orientation),n_orientation)';
        
        % orientation psuedo-randomly sampled (rejection sampling) such
        % that no pairs of orientations are the same
        % frequency changes must also be counter-balanced so that each
        % orientation-contrast repetition has each kind of frequency change
        ori = shufflenorepeats(4, n_orientation);
        con = NaN(length(ori),1);
        freq_change = NaN(length(ori),1);
        for i = 1:n_orientation
            ids = find(ori == i);
            con(ids) = Shuffle([.5,.5,1,1]);
            for c = [0.5,1]
                ids = find((ori==i)' & (con==c));
                freq_change(ids) = Shuffle(1:2);
            end
        end
        
        t.contrast = con;
        t.orientation = orientation(ori);
        t.freq_change_feature = freq_change;
        t.run = ones(size(t,1),1) * run;
        t.subject = ones(size(t,1),1) * subject;
        t.trial = (1:size(t,1))';
        t.n_isi_sec = [Shuffle(isi_available); final_blank_sec];
        T = [T; t];
    end
end

% determines length of stimulus presentation
T.n_stim_sec = ones(size(T,1),1)*n_stim_sec;

T.n_stim_flip = T.n_stim_sec * stim_hz;
T.n_isi_flip = T.n_isi_sec * stim_hz;

% on which flips will the event occur?
T.freq_change_flip = randsample((1*stim_hz):(n_stim_sec*stim_hz-1), size(T,1), true)';
T.freq_change_vbl = NaN(size(T,1),1);

% filled in during stimulus presentation
T.trial_start = NaN(size(T,1),1);
T.trial_end = NaN(size(T,1),1);

% filled in as participant responds (response is cell, which needs special
% handling during reading and so is created when preparing datatable)
T.rt = NaN(size(T,1),1);

% these are calculated from trial_*
T.onset = NaN(size(T,1),1);
T.duration = NaN(size(T,1),1);

writetable(T,'task-vtf_blocking.csv');


