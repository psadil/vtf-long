
hmax = 0.898;
hmin = 0.098;

lmax = 0.6;
lmin = 0.4;

load('D:\git\fMRI\VTF-long\stimuli\gammaTable-hmrc-factory-rgb.mat')

% rows in gammatable that were shown
rlmax = find(abs(sum(lmax - gammaTable,2)) == min(abs(sum(lmax - gammaTable,2))) );
rlmin = find(abs(sum(lmin - gammaTable,2)) == min(abs(sum(lmin - gammaTable,2))) );

rhmax = find(abs(sum(hmax - gammaTable,2)) == min(abs(sum(hmax - gammaTable,2))) );
rhmin = find(abs(sum(hmin - gammaTable,2)) == min(abs(sum(hmin - gammaTable,2))) );

% intensity fractions that were shown
ilmax = (rlmax-1)/256;
ilmin = (rlmin-1)/256;

ihmax = (rhmax-1)/256;
ihmin = (rhmin-1)/256;

% contrast values of these intensities
low_contrast = (ilmax - ilmin) / (ilmax + ilmin); % 0.4286
high_contrast = (ihmax - ihmin) / (ihmax + ihmin); % 0.9896

plot(linspace(0,1,256),gammaTable)
refline(1,0)
hold on
plot([0, 1], [lmin, lmin], 'k')
plot([ilmin, ilmin], [0, 1], 'k')

plot([0, 1], [lmax, lmax], 'k')
plot([ilmax, ilmax], [0, 1], 'k')

plot([0, 1], [hmin, hmin], 'k')
plot([ihmin, ihmin], [0, 1], 'k')

plot([0, 1], [hmax, hmax], 'k')
plot([ihmax, ihmax], [0, 1], 'k')



chmax = mean(gammaTable(ceil(hmax*255),:));
chmin = mean(gammaTable(ceil(hmin*255),:));
clmax = mean(gammaTable(lmax*255,:));
clmin = mean(gammaTable(lmin*255,:));


plot(linspace(0,1,256),gammaTable)
refline(1,0)
hold on
plot([lmin, lmin], [0, 1], 'k')
%plot([0, 1], [ilmin, ilmin], 'k')

plot([lmax, lmax], [0, 1], 'k')
%plot([ilmax, ilmax], [0, 1], 'k')

plot([hmin, hmin], [0, 1], 'k')
%plot([ihmin, ihmin], [0, 1], 'k')

plot([hmax, hmax],[0, 1], 'k')
%plot([ihmax, ihmax], [0, 1], 'k')

plot([0.5,0.5],[0, 1], 'k')
