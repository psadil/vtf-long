%% measure intensity values for some number of measurements for each phosphor individually

monitor = 'hmrc';

screens=Screen('Screens');
screenNumber=max(screens);

PsychDefaultSetup(2);

w=Screen('OpenWindow',screenNumber);

numMeasures = 6;
intensity_vals = 0:1/(numMeasures-1):1;
readings = zeros(3,numMeasures);

for channel_i = 1:3
    for intensity_j = 1:numMeasures
        RGB = zeros(1,3);
        RGB(channel_i) = intensity_vals(intensity_j);
        Screen('FillRect',w, 255*RGB);
        Screen('Flip',w)
        readings(channel_i,intensity_j) = GetNumber;
    end
end
Screen('CloseAll')

