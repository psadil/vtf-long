

load('workspace_retinotopyCaltsmash.mat');
load('fixationgrid.mat')
% load('D:\git\fMRI\VTF-long\stimuli\images.mat')
alpha = maskimages(:,:,100);

im = images{1};

ima = im(:,:,:,1);

imwrite(ima, 'stimuli/wedge.png', 'alpha', alpha+specialoverlay(:,:,4))
