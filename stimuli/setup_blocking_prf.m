
% no seed setting. trials are deterministic

stim_hz = 15;

% ----- multibar trials -----
mb = table();
mb.trial = (1:8)';
% there is initial 16 seconds before runs
% 4 second isi after first 3 and 5-7
% final stimulus has 20 second 'isi' to close out run. Defined like this to
% enable fixation task to continue
mb.n_isi_sec = [repelem(4,3), 16, repelem(4,3), 20]';
mb.direction = num2cell([0, 90, 180, 270, 45, 135, 225, 315])';
mb.wedgeorring = repmat({'bar'}, [size(mb,1),1]);
mb.n_stim_sec = ones(size(mb,1),1)*28;

% ----- wedgering trials -----
wr = table();
wr.trial = (1:8)';
% there is initial 16 seconds before runs
% these stimuli move between 'trials' immediately
% final stimulus has 22 second 'isi' to close out run. Defined like this to
% enable fixation task to continue (note different from above)
wr.n_isi_sec = [0;0;4;4;0;0;4;26];
wr.direction = [{'CCW'}, {'CCW'}, {'expand'}, {'expand'}, {'CW'}, {'CW'}, {'contract'}, {'contract'}]';
wr.wedgeorring = repmat([{'wedge'}, {'wedge'}, {'ring'},{'ring'}]', [2,1]);
wr.n_stim_sec = [32;32;28;28;32;32;28;28];

T = [mb; wr];
T.run_type = [repmat({'multibar'},[size(mb,1),1]); repmat({'wedgering'},[size(wr,1),1])];
% determines length of stimulus presentation

T.n_stim_flip = T.n_stim_sec * stim_hz;
T.n_isi_flip = T.n_isi_sec * stim_hz;

% filled in during stimulus presentation
T.trial_start = NaN(size(T,1),1);
T.trial_end = NaN(size(T,1),1);


% these are calculated from trial_*
T.onset = NaN(size(T,1),1);
T.duration = NaN(size(T,1),1);

writetable(T,'task-prf_blocking.csv');


