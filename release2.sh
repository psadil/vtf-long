#!/usr/bin/env bash
set -e
repo_state=$(git status --porcelain)

# If $repo_state is not empty
if [ -n "$repo_state" ]; then
  echo -e "Repo is dirty, ignoring changes in the following modified/untracked files:\n"
  echo -e "${repo_state}\n"
fi

sha=$(git rev-parse --short master)
echo "$sha" > "version.txt"

echo "Now zip and copy"
