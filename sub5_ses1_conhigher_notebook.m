sub = 5;

%% run on laptop (no need to finish entirely)
main('subject', sub, 'run', 0, 'exp', 'VTF', 'using_tracker', false, 'gamma', false, 'skipsynctests',2)
main('subject', sub, 'run', 0, 'exp', 'multibar', 'using_tracker', false, 'gamma', false, 'skipsynctests',2) 


%% run from scanner 
diary sub-05_ses-1_diary.txt
sub = 5;

main('subject', sub, 'run', 1, 'exp', 'calibrate') 

% starts with 
% 1) anatomical scan
% 1.5) Aligning of scans to calcarine
% 2) Fieldmapping scan 

main('subject', sub, 'run', 2, 'exp', 'calibrate') 

% single-band reference scan (~24 seconds)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 1, 'exp', 'multibar', 'gamma', false) 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 1, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 2, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 3, 'exp', 'VTF')

% single-band reference scan (~24 seconds)

% retinotopy tasks require 5 minutes
main('subject', sub, 'run', 1, 'exp', 'wedgering', 'gamma', false) 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 4, 'exp', 'VTF') 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 5, 'exp', 'VTF') 

% single-band reference scan (~24 seconds)

% task requires 490 seconds (8 minutes, 10 seconds)
main('subject', sub, 'run', 6, 'exp', 'VTF') 

diary off
